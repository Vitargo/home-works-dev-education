package com.bitbucket.list;

import com.bitbucket.list.exception.ListEmptyException;
import com.bitbucket.list.exception.PositionOutOfBoundListException;

import java.util.Objects;

public class LinkedListNextAndPrev implements IList {

    private Node start;

    private Node end;

    public static class Node {

        int value;

        Node next;

        Node prev;

        public Node(int value) {
            this.value = value;
        }
    }

    @Override
    public void init(int[] init) {
        if (Objects.nonNull(init)) {
            for (int i = init.length - 1; i >= 0 ; i--) {
                addStart(init[i]);
            }
        }
    }

    @Override
    public void clear() {
        this.start = this.end = null;
    }

    @Override
    public int size() {
        int count = 0;
        Node tmp = this.start;
        while (tmp != null) {
            count++;
            tmp = tmp.next;
        }
        return count;
    }

    @Override
    public int[] toArray() {
        int count = 0;
        int[] result = new int[size()];
        Node tmp = this.start;
        while (tmp != null) {
            result[count++] = tmp.value;
            tmp = tmp.next;
        }
        return result;
    }

    @Override
    public void addStart(int val) {
        if (Objects.isNull(this.start)) {
            this.start = this.end = new Node(val);
        } else {
            Node tmp = new Node(val);
            tmp.next = this.start;
            this.start = tmp;
            tmp.next.prev = tmp;
        }
    }

    @Override
    public void addEnd(int val) {
        if (Objects.isNull(this.start)) {
            this.start = this.end = new Node(val);
        } else {
            Node tmp = new Node(val);
            this.end.next = tmp;
            tmp.prev = this.end;
            this.end = tmp;
        }
    }

    @Override
    public void addByPos(int pos, int val) {
        int size = size();
        if(size == 0){
            throw new ListEmptyException();
        }
        if (pos < 0 || pos >= size) {
            throw new PositionOutOfBoundListException();
        }
        if (pos == 0) {
            addStart(val);
        } else {
            Node tmp = this.start;
            Node newNode = new Node(val);
            for (int i = 0; i < pos-1; i++){
                tmp = tmp.next;
            }
            newNode.next = tmp.next;
            newNode.prev = tmp;
            tmp.next = newNode;
            tmp.next.prev = newNode;
        }
    }

    @Override
    public int removeStart() {
        int size = size();
        int first;
        if (size == 0) {
            throw new ListEmptyException();
        } else if (size == 1) {
            first = this.start.value;
            this.start = this.end = null;
        } else {
            first = this.start.value;
            this.start = this.start.next;
            this.start.prev = null;
        }
        return first;
    }

    @Override
    public int removeEnd() {
        int size = size();
        int end;
        if (size == 0) {
            throw new ListEmptyException();
        } else if (size == 1) {
            end = this.start.value;
            this.start = this.end = null;
        } else {
            end = this.end.value;
            this.end = this.end.prev;
            this.end.next = null;
        }
        return end;
    }

    @Override
    public int removeByPos(int pos) {
        int size = size();
        int result;
        if (size == 0) {
            throw new ListEmptyException();
        }
        if (pos < 0 || pos >= size) {
            throw new PositionOutOfBoundListException();
        }
        if (pos == 0) {
            result = removeStart();
        } else if (pos == size-1) {
            result = removeEnd();
        } else {
            Node tmp = this.start;
            for (int i = 0; i < pos - 1; i++) {
                tmp = tmp.next;
            }
            result = tmp.next.value;
            tmp.next = tmp.next.next;
            tmp.next.prev = tmp.prev;
        }
        return result;
    }

    @Override
    public int max() {
        int size = size();
        if (size == 0) {
            throw new ListEmptyException();
        }
        int result = this.start.value;
        Node tmp = this.start;
        while(tmp.next != null){
            if(result < tmp.next.value){
                result = tmp.next.value;
            }
            tmp = tmp.next;
        }
        return result;
    }

    @Override
    public int min() {
        int size = size();
        if (size == 0) {
            throw new ListEmptyException();
        }
        int result = this.start.value;
        Node tmp = this.start;
        while(tmp.next != null){
            if(result > tmp.next.value){
                result = tmp.next.value;
            }
            tmp = tmp.next;
        }
        return result;
    }

    @Override
    public int maxPos() {
        int size = size();
        if (size == 0) {
            throw new ListEmptyException();
        }
        int result = this.start.value;
        int count = 0;
        int index = 0;
        Node tmp = this.start;
        while(tmp.next != null){
            count++;
            if(result < tmp.next.value){
                result = tmp.next.value;
                index = count;
            }
            tmp = tmp.next;
        }
        return index;
    }

    @Override
    public int minPos() {
        int size = size();
        if (size == 0) {
            throw new ListEmptyException();
        }
        int result = this.start.value;
        int count = 0;
        int index = 0;
        Node tmp = this.start;
        while(tmp.next != null){
            count++;
            if(result > tmp.next.value){
                result = tmp.next.value;
                index = count;
            }
            tmp = tmp.next;
        }
        return index;
    }

    @Override
    public int[] sort() {
        int size = size();
        if (size == 0) {
            throw new ListEmptyException();
        }
        Node current = this.start;
        Node index;
        int tmp;
        while(current != null){
            index = current.next;
            while(index != null){
                if (current.value > index.value){
                    tmp = current.value;
                    current.value = index.value;
                    index.value = tmp;
                }
                index = index.next;
            }
            current = current.next;
        }
        return toArray();
    }

    @Override
    public int get(int pos) {
        int size = size();
        if (size == 0) {
            throw new ListEmptyException();
        }
        if (pos < 0 || pos >= size) {
            throw new PositionOutOfBoundListException();
        }
        Node tmp;
        int result;
        if (pos == 0){
            result = this.start.value;
        } else {
            tmp = this.start;
            for (int i = 0; i < pos-1; i++) {
                tmp = tmp.next;
            }
            result = tmp.next.value;
        }
        return result;
    }

    @Override
    public int[] halfRevers() {
        int size = size();
        if (size == 0) {
            throw new ListEmptyException();
        }
        int half;
        if(size%2 == 0){
            half = size/2;
        } else {
            half = (size+1)/2;
        }
        for(int i = 0; i < half; i++){
            Node tmp = this.start;
            removeStart();
            addEnd(tmp.value);
        }
        return toArray();
    }

    @Override
    public int[] revers() {
        int size = size();
        if (size == 0) {
            throw new ListEmptyException();
        }
        Node current = this.start;
        Node tmp = null;
        while (current != null){
            tmp = current;
            current = current.next;
            tmp.next = tmp.prev;
            tmp.prev = current;
        }
        this.start = tmp;
        return toArray();
    }

    @Override
    public void set(int pos, int val) {
        int size = size();
        if (size == 0) {
            throw new ListEmptyException();
        }
        if (pos < 0 || pos >= size) {
            throw new PositionOutOfBoundListException();
        }
        if (pos == 0) {
            this.start.value = val;
        } else if(pos == size-1){
            this.end.value = val;
        } else {
            Node tmp = this.start;
            for (int i = 0; i < pos - 1; i++) {
                tmp = tmp.next;
            }
            tmp.next.value = val;
        }
    }
}
