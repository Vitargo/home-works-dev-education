package com.bitbucket.bitree;

import java.util.Objects;

public class AVLTreeRecursive implements ITree{

    private Node root;

    private static class Node {

        int value;

        int height;

        Node right;

        Node left;

        public Node(int value) {
            this.value = value;
            height = 0;
        }
    }

    int height(Node node) {
        if (node == null){
            return -1;
        } else {
            return node.height;
        }
    }

    // LEFT

    public Node rotateWithLeft(Node c) {
        Node p;

        p = c.left;
        c.left = p.right;
        p.right = c;

        c.height = Math.max(height(c.left), height(c.right)) + 1;
        p.height = Math.max(height(p.left), height(p.right)) + 1;

        return p;
    }

    // RIGHT

    public Node rotateWithRight(Node c) {
        Node p;

        p = c.right;
        c.right = p.left;
        p.left = c;

        c.height = Math.max(height(c.left), height(c.right)) + 1;
        p.height = Math.max(height(p.left), height(p.right)) + 1;

        return p;
    }

    // DOUBLE LEFT

    public Node doubleRotateWithLeft(Node c) {
        Node tmp;

        c.left = rotateWithRight(c.left);
        tmp = rotateWithLeft(c);

        return tmp;
    }

    // DOUBLE RIGHT

    public Node doubleRotateWithRight(Node c) {
        Node tmp;

        c.right = rotateWithLeft(c.right);
        tmp = rotateWithRight(c);

        return tmp;
    }

    @Override
    public void init(int[] init) {
        if (Objects.isNull(init)) {
            init = new int[0];
        }
        for (int elem : init) {
            add(elem);
        }
    }

    @Override
    public void clear() {
        this.root = null;
    }

    @Override
    public void add(int val) {
        Node newNode = new Node(val);
        if(this.root == null) {
            this.root = newNode;
        } else {
            this.root = addNode(newNode, this.root);
        }
    }

    public Node addNode(Node newNode, Node par) {
        Node newpar = par;
        if (newNode.value < par.value) {
            if (par.left == null) {
                par.left = newNode;
            } else {
                par.left = addNode(newNode, par.left);
                if ((height(par.left) - height(par.right)) == 2) {
                    if (newNode.value < par.left.value) {
                        newpar = rotateWithLeft(par);
                    } else {
                        newpar = doubleRotateWithLeft(par);
                    }
                }
            }
        } else if (newNode.value > par.value) {
            if (par.right == null) {
                par.right = newNode;
            } else {
                par.right = addNode(newNode, par.right);
                if ((height(par.right) - height(par.left)) == 2) {
                    if (newNode.value > par.right.value) {
                        newpar = rotateWithRight(par);
                    } else {
                        newpar = doubleRotateWithRight(par);
                    }
                }
            }
        }
        if ((par.left == null) && (par.right != null)){
            par.height = par.right.height + 1;
        } else if ((par.right == null) && (par.left != null)){
            par.height = par.left.height + 1;
        } else {
            par.height = Math.max(height(par.left), height(par.right)) + 1;
        }
        return newpar;
    }

    @Override
    public int size() {
        return sizeNode(this.root);
    }

    public int sizeNode(Node p) {
        if (p == null) {
            return 0;
        }
        int count = 0;
        count += sizeNode(p.right);
        count++;
        count += sizeNode(p.left);
        return count;
    }

    @Override
    public int leaves() {
        return countNodeLeaves(this.root);
    }
    public int countNodeLeaves(Node node){
        if(node == null) {
            return 0;
        }
        if(node.left == null && node.right == null){
            return 1;
        } else {
            return countNodeLeaves(node.left) + countNodeLeaves(node.right);
        }
    }

    @Override
    public int nodes() {
        return countNodes(this.root);
    }

    public int countNodes(Node node) {
        if (node == null){
            return 0;
        } else {
            return 1 + countNodes(node.left) + countNodes(node.right);
        }
    }

    @Override
    public int height() {
        if (this.root == null)
            return -1;
        int leftHeight = height(root.left);
        int rightHeight = height(root.right);

        return Math.max(leftHeight, rightHeight) + 1;
    }

    @Override
    public int width() {
        return 0;
    }

    @Override
    public void reverse() {

    }

    @Override
    public void delete(int val) {

    }

    @Override
    public int[] toArray() {
        if (Objects.isNull(this.root)) {
            return new int[0];
        }
        int[] array = new int[size()];
        Counter c = new Counter();
        toArrayNode(array, c, this.root);
        return array;
    }

    @Override
    public void print() {
        inOrder(this.root);
    }

    public void inOrder(Node node) {
        if (node == null) {
            return;
        }
        inOrder(node.left);
        System.out.printf("%s ", node.value);
        inOrder(node.right);
    }

    public void toArrayNode(int[] array, Counter c, Node node) {
        if (Objects.isNull(node)) {
            return;
        }
        toArrayNode(array, c, node.left);
        array[c.index++] = node.value;
        toArrayNode(array, c, node.right);
    }

    private static class Counter {
        int index = 0;
    }
}
