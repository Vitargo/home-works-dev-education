package orb.bitbucket.http.threadpool;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.concurrent.*;

public class Main {

    public static void main(String[] args) throws IOException {

        final ServerSocket serverSocket = new ServerSocket(8082);
        LinkedBlockingDeque<Runnable> linkedBlockingDeque = new LinkedBlockingDeque<>();
        final ExecutorService executorService =
                new ThreadPoolExecutor(5, 10, 0L, TimeUnit.MILLISECONDS,
                        linkedBlockingDeque);
        System.out.println("Server started with port " + 8082);

        while (true) {
            final Socket socket = serverSocket.accept();
            System.out.println("new http request received from: " + serverSocket.getInetAddress().getHostAddress());

            if (socket.getInputStream().available() > 0) {
                HttpHandler httpHandler = new HttpHandler(socket);

                executorService.execute(httpHandler);

                System.out.println(linkedBlockingDeque.size());

            } else {
                socket.close();
                continue;
            }

        }
    }
}
