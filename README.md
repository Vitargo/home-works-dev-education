# Home works

## HW 0

## Conditional operators

1. If a - even count a * b, otherwise a + b.
2. Determine which quarter belongs to the point with coordinates (x,y).
3. Find sum of only positive number of three given numbers.
4. Calculate max(a*b*c, a+b+c)+3.
5. Write a grading program based on the student’s rating according the following table.

   Rate | Result
:------:|:------:
  0-19  |   F
  20-39 |   E
  40-59 |   D
  60-74 |   C
  75-89 |   B
  90-100|   A

  
## Loops

1. Find the sum of even numbers and count them in the range from 1 to 99.
2. Check for a prime number?
3. Find the root of a natural number to the nearest whole (consider and binary search method).
4. Calculate the factorial of n. n! = 1*2*...*n-1*n;!
5. Calculate the sum of the digits of a given number.
6. Output a number that is a mirror reflection of a digit of a given number, for example, given the number 123, output 321.

## Arrays

1. Find minimum array element.
2. Find maximum array element.
3. Find the minimum array element index.
4. Find maximum array element index.
5. Calculate the sum of odd-numbered array elements.
6. Reverse array.
7. Count odd array elements.
8. Swap the first and second half of the array, for example for array 1 2 3 4, result 3 4 1 2.
9. Sort array (Bubble Sort, Select Sort, Insert Sort).
10. Sort array (Quick Sort, Merge Sort, Shell Sort, Heap Sort).

## Methods

1. Get the line name of the day of the week by the number of the day.
2. Enter a number (0-999) and you get a string with a number as text.
3. Enter a string that contains the number as text (0-999) and you get a number of digits.
4. Find the distance between two points in a two-dimensional Cartesian space.
5. Enter a number (0-999 billions) and you get a string with a number as text.
6. Enter a string that contains the number as text (0-999 billions) and you get a number of digits.
---

## HW 1

## Application “Randomizer”

Консольное приложение, при запуске которого пользователю предлагается установить диапазон случайных чисел рандомайзера путем ввода минимального и максимального чисел, предположим (min = 1, max = 100). Валидация: числа не должны быть отрицательными. Минимальное стартовое число диапазона должно быть больше либо равно 1. Максимальное число должно быть меньше либо равно 500. Числа с плавающей точкой запрещены.
После установки min и max чисел диапазона у пользователя должно быть в арсенале только три команды: “generate”, “exit”, “help”. Последняя команда работает как обычный хелпер (путеводитель) для пользователя, реализация на усмотрение исполнителя.
При вводе команды “exit” - приложение закрывается, пользователю сообщается, что приложение больше не активно (Optional - можно обработать дополнительным вопросом: “Are you sure that you want to quit the app?”).
При вводе команды “generate” - приложение генерирует в консоль случайное число которое находится в заданном пользователем диапазоне при старте. При каждой генерации нового числа - числа не повторяются. Другими словами, числа уникальны и могут появится только единожды.

---

## HW 2

## Application “Guess Number”

Консольное приложение, при запуске которого пользователю предлагается угадать число от 1 до 100 за 5 попыток. Конструктор класса запуска аппликации должен предусматривать варианты настроек диапазона чисел (валидация: не отрицательные, без плавающей точки, минимум = 1, максимум = 200) и количества попыток (валидация: не отрицательные, без плавающей точки, минимум = 1, максимум = 15).
Игра начинается с того, что пользователь видит сообщение: “Привет, я загадал число от min до max вашего диапазона. Попробуй угадать его за x попыток!” И пользователю предлагается использовать свою первую попытку путём ввода первого числа в консоль и нажать Enter.
Примечание: в любой момент времени, пользователь может прекратить игру и ввести сообщение “exit”, после чего игра моментально остановится. Все остальные команды никак не валидируются, на вход принимаются только числа.
Со второй попытки для пользователя вводятся подсказки, типа: “Не угадал, но теплее!!! Осталось n попыток” либо “Не угадал, холоднее… Осталось n попыток”.
Если пользователь угадал число - выводится сообщение “Поздравляю! Ты угадал задуманное число за n попыток”.
Остальные мелкие доработки - на усмотрение разработчика.

## HW 5

## 5 Тема: библиотека Math
 
1. 	Стрельба из гаубицы. Дан угол возвышения ствола а и начальная скорость полёта снаряда. v км/ч. Вычислить расстояние полёта снаряда. Реализовать решения для угла в градусах и в радианах.
2. 	Скорость первого автомобиля v1 км/ч, второго — v2 км/ч, расстояние между ними s км. Какое расстояние будет между ними через t ч, если автомобили 	движутся в разные стороны?
3. 	Записать логическое выражение, принимающее значение 1, если точка лежит внутри заштрихованной области, иначе — 0. 
![Test Image 1](https://bitbucket.org/Vitargo/home-works-dev-education/src/master/pictures/task4.png)
4. 	Вычислить значение выражения
![Test Image 2](https://bitbucket.org/Vitargo/home-works-dev-education/src/master/pictures/task5.png)

## Тема 5: случайные числа – генератор случайных чисел.

1. Вывести на консоль случайное число.
2. Вывести на консоль 10 случайных чисел.
3. Вывести на консоль 10 случайных чисел, каждое в диапазоне от 0 до 10.
4. Вывести на консоль 10 случайных чисел, каждое в диапазоне от 20 до 50.
5. Вывести на консоль 10 случайных чисел, каждое в диапазоне от -10 до 10.
6. Вывести на консоль случайное количество (в диапазоне от 3 до 15) случайных чисел, каждое в диапазоне от -10 до 35.

## Drawing Stars Figures
![Test Image 1]


