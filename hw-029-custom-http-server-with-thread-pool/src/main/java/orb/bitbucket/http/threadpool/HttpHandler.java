package orb.bitbucket.http.threadpool;

import java.io.*;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.nio.charset.StandardCharsets;

public class HttpHandler implements Runnable{

    private SimpleHttpRequest req;

    private SimpleHttpResponse resp;

    private BufferedWriter bw;

    public HttpHandler(Socket socket) throws IOException {
        this.req = new SimpleHttpRequest(socket);
        this.resp = new SimpleHttpResponse();
        this.bw = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
    }

    public void formatResponse() throws IOException {
        String typeRequest = this.req.getRequestType();
        if(typeRequest.equals("GET")){
            doGet(this.req, this.resp);
        } else if (typeRequest.equals("PUT")){
            doPut(this.req, this.resp);
        } else if (typeRequest.equals("POST")){
            doPost(this.req, this.resp);
        } else if (typeRequest.equals("DELETE")) {
            doDelete(this.req, this.resp);
        } else {
            doOther(this.req, this.resp);
        }
    }

    public void doGet(SimpleHttpRequest req, SimpleHttpResponse resp) throws IOException {
        String response = SimpleHttpResponse.responseOnGet();
        bw.write(response);
        bw.flush();
    }

    public void doPost(SimpleHttpRequest req, SimpleHttpResponse resp) throws IOException {
        String body = req.getBody();
        String name = getName(body);
        String greeting = getGreetings(body);
        String response = SimpleHttpResponse.responseOnPost(name, greeting);
        bw.write(response);
        bw.flush();
    }

    public void doPut(SimpleHttpRequest req, SimpleHttpResponse resp) throws IOException {
        String response = SimpleHttpResponse.responseOnPut();
        bw.write(response);
        bw.flush();
    }

    public void doDelete(SimpleHttpRequest req, SimpleHttpResponse resp) throws IOException {
        String response = SimpleHttpResponse.responseOnDelete();
        bw.write(response);
        bw.flush();
    }

    public void doOther(SimpleHttpRequest req, SimpleHttpResponse resp) throws IOException {
        String response = SimpleHttpResponse.responseOnOther();
        bw.write(response);
        bw.flush();
    }

    public static String getName(String body) {
        int str = body.indexOf("&to=");
        String name = body.substring(str+4);
        return name;
    }

    public static String getGreetings(String body) {
        int f = body.indexOf("&to=");
        int s = body.indexOf("say=");
        String name = body.substring(s+4, f);
        return name;
    }

    @Override
    public void run() {
        try {
            formatResponse();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
