package org.bitbucket.balls;

import javax.swing.*;
import java.awt.*;
import java.util.Objects;
import java.util.Random;
import java.util.Set;

public class Ball extends JPanel implements Runnable {

    private final Random rnd = new Random();

    private int dx;

    private int dy;

    private Color clr;

    private int radius;

    private final Point point;

    private BallsPanel panel;

    public Ball(Point point, BallsPanel panel) {
        this.point = point;
        this.clr = randomColor();
        this.radius = randomRadius();
        this.dx = this.rnd.nextInt(8)-3;
        this.dy = this.rnd.nextInt (8)-3;
        this.panel = panel;
        setSize(100, 100);
        setOpaque(Boolean.FALSE);
    }

    private void move() throws InterruptedException {
        JPanel pan = (JPanel) getParent();
        panel.addPointToList(this, Thread.currentThread().getName());

        if (BallsPanel.getCloseThread().contains(this)){
            BallsPanel.getCloseThread().remove(this);
            stopThreadByName(Thread.currentThread().getName());
            panel.remove(this);
            panel.repaint();
        }
        if (this.point.x <= 0 || this.point.x + radius >= pan.getWidth()) {
            dx = -dx;
        }
        if (this.point.y <= 0 || this.point.y + radius >= pan.getHeight()) {
            dy = -dy;
        }
        this.point.translate(dx, dy);
        setLocation(point);
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2d = (Graphics2D) g;
        g2d.setColor(clr);
        g2d.fillOval(0,0,radius,radius);
    }

    @Override
    public void run() {
        try {
            while (true) {
                move();
                Thread.sleep(10);
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupted();
            BallsPanel.setCounterBalls(BallsPanel.getCounterBalls()-1);
        }
    }

    public Color randomColor(){
        int r,g,b;
        r = (int)(255 * Math.random());
        g = (int)(255 * Math.random());
        b = (int)(255 * Math.random());
        Color c = new Color(r,g,b);
        return c;
    }

    public int randomRadius(){
        Random random = new Random();
        int number = random.nextInt(50 - 10 + 1) + 10;
        return number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ball ball = (Ball) o;
        return dx == ball.dx && dy == ball.dy && radius == ball.radius && Objects.equals(rnd, ball.rnd) && Objects.equals(clr, ball.clr) && Objects.equals(point, ball.point) && Objects.equals(panel, ball.panel);
    }

    @Override
    public int hashCode() {
        return Objects.hash(rnd, dx, dy, clr, radius, point, panel);
    }

    @Override
    public String toString() {
        return "Ball{" +
                "rnd=" + rnd +
                ", dx=" + dx +
                ", dy=" + dy +
                ", clr=" + clr +
                ", radius=" + radius +
                ", point=" + point +
                ", panel=" + panel +
                '}';
    }

    public int getRadius() {
        return radius;
    }

    public int getX() {
        return point.x;
    }

    public int getY() {
        return point.y;
    }

    public int getDx() {
        return dx;
    }

    public int getDy() {
        return dy;
    }

    public void setDx(int dx) {
        this.dx = dx;
    }

    public void setDy(int dy) {
        this.dy = dy;
    }

    public Color getClr() {
        return clr;
    }
    public void setClr(Color color) {
        this.clr = color;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    private void stopThreadByName(String threadName) {
        Set<Thread> setOfThread = Thread.getAllStackTraces().keySet();
        for(Thread thread : setOfThread){
            if(thread.getName() == threadName){
                thread.interrupt();
            }
        }
    }
}
