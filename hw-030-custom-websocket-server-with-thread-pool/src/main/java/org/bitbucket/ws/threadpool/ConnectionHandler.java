package org.bitbucket.ws.threadpool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.security.NoSuchAlgorithmException;

public class ConnectionHandler implements Runnable{

    private final Logger log = LoggerFactory.getLogger(ConnectionHandler.class);

    private Socket socket;

    public ConnectionHandler(Socket socket) {
        this.socket = socket;
    }

    public void run() {
        InputStream in = null;
        OutputStream out = null;
        try {
            in = socket.getInputStream();
            out = socket.getOutputStream();
        } catch (IOException e) {
            log.error("Enter {}:" + e);
        }
        try {
            WebsocketHandler.doHandShake(in, out);

            out.write(WebsocketHandler.encode("Hello from Server!"));
            out.flush();

            WebsocketHandler.printInputStream(in);
        } catch (IOException | NoSuchAlgorithmException e) {
            log.error("Enter {}:" + e);
        }

        while(true) {
            String line = null;
            try {
                line = WebsocketHandler.printInputStream(in);
                if (line == null) break;
                System.out.println("Echo: " + line);
                out.write(WebsocketHandler.encode("Echo: " + line));
            } catch (SocketTimeoutException e) {
                log.warn("Connection timed out");
                log.error("Enter {}:" + e);
            } catch (IOException e) {
                log.error("Enter {}:" + e);
            }
        }
    }
}
