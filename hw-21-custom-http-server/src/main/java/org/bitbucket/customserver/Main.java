package org.bitbucket.customserver;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

public class Main {
    public static void main(String[] args) throws IOException {

        ServerSocket serverSocket = new ServerSocket(8082);
        System.out.println("Server started with port " + 8082);

        while (true) {
            Socket socket = serverSocket.accept();
            System.out.println("new http request received from: " + serverSocket.getInetAddress().getHostAddress());

            if (socket.getInputStream().available() > 0) {
                MyHttpHandler handler = new MyHttpHandler(socket);
                handler.formatResponse();
            } else {
                socket.close();
                continue;
            }
            System.out.println("Socket closed.");
            socket.close();
        }
    }
}
