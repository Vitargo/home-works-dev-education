package org.bitbucket.customserver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

public class SimpleHttpRequest {

    private final Map<String, String> header;

    public SimpleHttpRequest(Socket socket) {
        this.header = parseRequest(socket);
    }

    public String getRequestType(){
        return this.header.get("TypeRequest:");
    }

    public String getBody() {
        return this.header.get("Body:");
    }

    public Map<String, String> parseRequest(Socket socket) {
        Map<String, String> header = new HashMap<>();
        InputStream in;
        try {
            in = socket.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            int count = 0;
            String[] k_v;
            String str = br.readLine();
            String body;
            int k = -1;
                while (str != null && !str.isEmpty()) {
                    System.out.println(str);
                    count++;
                    if (count == 1) {
                        k_v = str.split(" ", 2);
                        header.put(k_v[0], k_v[1]);
                        header.put("TypeRequest:", k_v[0]);
                    } else {
                        k_v = str.split(":", 2);
                        header.put(k_v[0], k_v[1]);
                    }
                    if (k_v[0].equals("Content-Length")) {
                        int i = str.indexOf("Content-Length");
                        if (i != -1) {
                            k = Integer.parseInt(str.substring(i + 16));
                        }
                    }
                    str = br.readLine();
                }

            if (k != -1) {
                    char[] chars = new char[k];
                    System.out.println(k);
                    br.read(chars);
                    body = String.valueOf(chars);
                    header.put("Body:", body);
                }
            } catch(IOException e){
                e.printStackTrace();
            }
        return header;
    }
}

