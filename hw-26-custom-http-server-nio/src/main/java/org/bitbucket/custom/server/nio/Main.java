package org.bitbucket.custom.server.nio;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;

public class Main {

    public static void main(String[] args) throws IOException {

        ServerSocketChannel serverSocket;
        SocketChannel client;
        serverSocket = ServerSocketChannel.open();
        serverSocket.socket().bind(new InetSocketAddress(8083));
        System.out.println("Server started with port " + 8083);

        while (true) {
            client = serverSocket.accept();
            System.out.println("ConnectionSet: " + client.getRemoteAddress());
            HttpHandler handler = new HttpHandler(client);
            handler.formatResponse();
            System.out.println("Socket closed.");
            client.close();
        }
    }
}