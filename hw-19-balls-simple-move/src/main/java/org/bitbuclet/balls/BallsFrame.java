package org.bitbuclet.balls;

import javax.swing.*;
import java.awt.*;

public class BallsFrame extends JFrame {

    public BallsFrame() throws HeadlessException {
        setLayout(null);
        setSize(900, 650);
        BallsPanel ballsPanel = new BallsPanel();
        ballsPanel.setBounds(10,10,800,600);
        ballsPanel.setBorder(BorderFactory.createStrokeBorder(new BasicStroke(5.0f)));
        add(ballsPanel);
        setVisible(Boolean.TRUE);
    }
}

