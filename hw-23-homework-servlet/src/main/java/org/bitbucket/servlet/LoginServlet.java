package org.bitbucket.servlet;


import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;

import javax.servlet.http.*;

@WebServlet(
        name = "LoginServlet",
        description = "A sample annotated servlet",
        urlPatterns = {"/Login"}
)
public class LoginServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("Enter doPost LoginServlet");
        String uname = request.getParameter("username");
        String pword = request.getParameter("password");
        response.setContentType("text/html");
        PrintWriter writer = response.getWriter();

        if (AuthHelper.isAllowed(uname, pword)) {
            response.setStatus(200);
            HttpSession session = request.getSession();
            session.setAttribute("user", "Vita");
            session.setMaxInactiveInterval(30*60);
            Cookie userName = new Cookie("user", uname);
            userName.setMaxAge(30*60);
            response.addCookie(userName);
            RequestDispatcher dispatcher = request.getRequestDispatcher("/goal");
            dispatcher.forward(request, response);
        } else {
            response.setStatus(403);
            RequestDispatcher rd = getServletContext().getRequestDispatcher("/Login.jsp");
            PrintWriter out= response.getWriter();
            out.println("<font color=red>Either user name or password is wrong. Status " + response.getStatus() + "! </font>");
            rd.include(request, response);
        }
    }
}