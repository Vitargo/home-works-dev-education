package com.bitbucket.bitree;

public class AVLTreeIteration implements ITree{

    private Node root;

    private static class Node {

        int value;

        int height;

        Node right;

        Node left;

        public Node(int value) {
            this.value = value;
            height = 1;
        }
    }

    int height(Node node) {
        if (node == null){
            return 0;
        }
        return node.height;
    }

    int max(int a, int b) {
        return Math.max(a, b);
    }

    Node rightRotate(Node y) {
        Node x = y.left;
        Node T2 = x.right;

        // Perform rotation
        x.right = y;
        y.left = T2;

        // Update heights
        y.height = max(height(y.left), height(y.right)) + 1;
        x.height = max(height(x.left), height(x.right)) + 1;

        // Return new root
        return x;
    }

    Node leftRotate(Node x) {
        Node y = x.right;
        Node T2 = y.left;

        // Perform rotation
        y.left = x;
        x.right = T2;

        //  Update heights
        x.height = max(height(x.left), height(x.right)) + 1;
        y.height = max(height(y.left), height(y.right)) + 1;

        // Return new root
        return y;
    }

    // Get Balance factor of node N
    int getBalance(Node node) {
        if (node == null)
            return 0;

        return height(node.left) - height(node.right);
    }

    @Override
    public void init(int[] init) {

    }

    @Override
    public void clear() {

    }

    @Override
    public void add(int val) {

    }

    @Override
    public int size() {
        return 0;
    }

    @Override
    public int leaves() {
        return 0;
    }

    @Override
    public int nodes() {
        return 0;
    }

    @Override
    public int height() {
        return 0;
    }

    @Override
    public int width() {
        return 0;
    }

    @Override
    public void reverse() {

    }

    @Override
    public void delete(int val) {

    }

    @Override
    public int[] toArray() {
        return new int[0];
    }

    @Override
    public void print() {

    }
}
