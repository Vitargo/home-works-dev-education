let data = [
    {
        "username": "Jeannie_23",
        "firstName": "Jeannie",
        "lastName": "Randolph",
        "password": "$3z&Se8Ph8",
    },
    {
        "username": "k_brown",
        "firstName": "Kathie",
        "lastName": "Brown",
        "password": "*7Gf5iNy7@",
    },
    {
        "username": "mega_powers",
        "firstName": "Powers",
        "lastName": "Justice",
        "password": "8*DkE87jb&",
    },
    {
        "username": "o.neal",
        "firstName": "Morales",
        "lastName": "Oneal",
        "password": "68Zp6-@vUp",
    },
    {
        "username": "A-dela",
        "firstName": "Adela",
        "lastName": "Heath",
        "password": "83-2pgJSj$",
    }];
const root = document.getElementById('root');

let br = document.createElement('br');

let form = document.createElement('form');
form.setAttribute('name', 'AuthForm')
form.setAttribute('class', 'form');
form.setAttribute('method', 'post');
form.setAttribute('onsubmit', 'auth()');

let FN = document.createElement('input');
FN.setAttribute('type', 'text');
FN.setAttribute('class', 'form__input');
FN.setAttribute('name', 'firstName');
FN.setAttribute('placeholder', 'First Name');
FN.setAttribute('maxlength', '10');
FN.setAttribute('required', '');

let error_FN = document.createElement('div');
error_FN.setAttribute('class', 'error');
error_FN.setAttribute('name', 'error_firstName');

let LN = document.createElement('input')
LN.setAttribute('type', 'text');
LN.setAttribute('class', 'form__input');
LN.setAttribute('name', 'lastName');
LN.setAttribute('placeholder', 'Last Name');
LN.setAttribute('maxlength', '20');
LN.setAttribute('required', '');

let error_LN = document.createElement('div');
error_LN.setAttribute('class', 'error');
error_LN.setAttribute('name', 'error_lastName');

let span = document.createElement('span');
span.setAttribute('class', 'inline');

let PSW = document.createElement('input');
PSW.setAttribute('type', 'password');
PSW.setAttribute('class', 'form__input');
PSW.setAttribute('name', 'password');
PSW.setAttribute('placeholder', 'Password');
PSW.setAttribute('maxlength', '10');
PSW.setAttribute('required', '');
PSW.setAttribute('style', 'margin-left: 138px');

let checkbox = document.createElement('input');
checkbox.setAttribute('id', 'checkbox');
checkbox.setAttribute('type', 'checkbox');
checkbox.setAttribute('onclick', 'showPassword()');

let label = document.createElement('label');
label.setAttribute('for','checkbox');
label.appendChild(document.createTextNode('Show password'));


let error_PSW = document.createElement('div');
error_PSW.setAttribute('class', 'error');
error_PSW.setAttribute('name', 'error_password');

let btn = document.createElement('input');
btn.setAttribute('type', 'submit');
btn.setAttribute('class', 'form__btn');
btn.setAttribute('value', 'Authorize');


form.appendChild(FN);
form.appendChild(error_FN);
form.appendChild(br.cloneNode());
form.appendChild(LN);
form.appendChild(error_LN);
form.appendChild(br.cloneNode());
form.appendChild(span);
span.appendChild(PSW);
span.appendChild(checkbox);
span.appendChild(label);
form.appendChild(error_PSW);
form.appendChild(br.cloneNode());
form.appendChild(btn);

root.appendChild(form);

let firstName = document.forms['AuthForm']['firstName'];
let lastName = document.forms['AuthForm']['lastName'];
let password = document.forms['AuthForm']['password'];

FN.addEventListener('input', validateFN);
LN.addEventListener('input', validateLN);
PSW.addEventListener('input', validatePSW);


function auth() {
     let result = false;
        let nickName;
        for(let i=0; i<data.length; i++){
            if (data[i].firstName === firstName.value && data[i].lastName === lastName.value && data[i].password === password.value){
                result = true;
                nickName = data[i].username;
                break;
            }
        }
        if (result){
            window.alert('Successful authorization for ' + nickName + ' ' + firstName.value + ' ' + lastName.value);
        } else {
            window.alert('Unsuccessful authorization!');
        }
}

function validateFN() {
    let content = error_FN.firstChild;
    if(content !== null){
        error_FN.firstChild.remove();
    };
    if (firstName.value === '') {
        content = document.createTextNode('Please, enter a Firstname!');
        error_FN.appendChild(content);
        firstName.style.border="2px solid red";
        error_FN.style.display = "block";
        firstName.focus();
    } else if (firstName.value.length < 2) {
        content = document.createTextNode('The Firstname must contain between 2 and 10 characters!');
        error_FN.appendChild(content);
        firstName.style.border = "2px solid red";
        error_FN.style.display = "block";
        firstName.focus();
    } else if (!firstName.value.match(/^[a-zA-Z\-]+$/)) {
        content = document.createTextNode('The Lastname must contain only letters and hyphens!');
        error_FN.appendChild(content);
        firstName.style.border="2px solid red";
        error_FN.style.display = "block";
        firstName.focus();
    } else {
        firstName.style.border="2px solid green";
        error_FN.style.display = "none";
    }
}

function validateLN () {
    let content = error_LN.firstChild;
    if(content !== null){
        error_LN.firstChild.remove();
    };
    if (lastName.value === '') {
        content = document.createTextNode('Please, enter a Lastname!');
        error_LN.appendChild(content);
        lastName.style.border="2px solid red";
        error_LN.style.display = "block";
        lastName.focus();
    } else if (lastName.value.length < 2) {
        content = document.createTextNode('The Lastname must contain between 2 and 20 characters!');
        error_LN.appendChild(content);
        lastName.style.border = "2px solid red";
        error_LN.style.display='block';
        lastName.focus();
    } else if (!lastName.value.match(/^[a-zA-Z\-]+$/)) {
        content = document.createTextNode('The Lastname must contain only letters and hyphens!');
        error_LN.appendChild(content);
        lastName.style.border = "2px solid red";
        error_LN.style.display='block';
        lastName.focus();
    } else {
        lastName.style.border="2px solid green";
        error_LN.style.display = "none";
    }
}

function validatePSW () {
    let content = error_PSW.firstChild;
    if(content !== null){
        error_PSW.firstChild.remove();
    };
    if (password.value === '') {
        content = document.createTextNode('Please, enter a Password!');
        error_PSW.appendChild(content);
        password.style.border="2px solid red";
        error_PSW.style.display = "block";
        password.focus();
    } else if (password.length < 5) {
        content = document.createTextNode('The Password must contain between 5 and 10 characters!');
        error_PSW.appendChild(content);
        password.style.border = "2px solid red";
        error_PSW.style.display='block';
        password.focus();
    } else if (!password.value.match(/(?=.*[A-Z])(?=.*[0-9])[A-Za-z\d$&@*-]{5,10}$/)) {
        content = document.createTextNode('The Password must contain letters, numbers, hyphens, ' +
            'special characters - $, &, @, *, at least one letter must be in the top case, at least ' +
            'one digit must be present!');
        error_PSW.appendChild(content);
        password.style.border = "2px solid red";
        error_PSW.style.display='block';
        password.focus();
    } else {
        password.style.border="2px solid green";
        error_PSW.style.display = "none";
    }
}

function showPassword() {
    if (password.type === "password") {
        password.type = "text";
    } else {
        password.type = "password";
    }
}