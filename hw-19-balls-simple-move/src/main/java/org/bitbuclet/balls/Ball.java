package org.bitbuclet.balls;

import javax.swing.*;
import java.awt.*;
import java.util.Random;

public class Ball extends JPanel implements Runnable {

    private final Random rnd = new Random();

    private int dx;

    private int dy;

    private final Color clr;

    private final int radius;

    private final Point point;

    public Ball(Point point) {
        this.clr = randomColor();
        this.radius = randomRadius();
        this.point = point;
        this.dx = this.rnd.nextInt(10)-5;
        this.dy = this.rnd.nextInt (10)-5;
        setSize(70,70);
        setOpaque(Boolean.FALSE);
    }

    private void move() {
        JPanel pan = (JPanel) getParent();
        if (this.point.x <= 0 || this.point.x + radius >= pan.getWidth()) {
            dx = -dx;
        }
        if (this.point.y <= 0 || this.point.y + radius >= pan.getHeight()) {
            dy = -dy;
        }
        this.point.translate(dx, dy);
        setLocation(point);

    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2d = (Graphics2D) g;
        g2d.setColor(clr);
        g2d.fillOval(1,1,radius,radius);
    }

    @Override
    public void run() {
        try {
            while (true) {
                move();
                Thread.sleep(10);
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupted();
        }
    }

    public Color randomColor(){
        int r,g,b;
        r = (int)(255 * Math.random());
        g = (int)(255 * Math.random());
        b = (int)(255 * Math.random());
        Color c = new Color(r,g,b);
        return c;
    }

    public int randomRadius(){
        Random random = new Random();
        return random.nextInt(50 - 10 + 1) + 10;
    }
}
