package com.bitbucket.bitree;

import java.util.Objects;

public class BinaryTreeRecursion implements ITree {

    private Node root;

    private static class Node {

        int value;

        Node right;

        Node left;

        public Node(int value) {
            this.value = value;
        }
    }

    @Override
    public void init(int[] init) {
        if (Objects.isNull(init)) {
            init = new int[0];
        }
        for (int elem : init) {
            add(elem);
        }
    }

    @Override
    public void clear() {
        this.root = null;
    }

    @Override
    public void add(int val) {
        if (Objects.isNull(this.root)) {
            this.root = new Node(val);
        } else {
            addNode(val, this.root);
        }
    }

    private void addNode(int val, Node p) {
        if (val < p.value) {
            if (Objects.isNull(p.left)) {
                p.left = new Node(val);
            } else {
                addNode(val, p.left);
            }
        } else if (val > p.value) {
            if (Objects.isNull(p.right)) {
                p.right = new Node(val);
            } else {
                addNode(val, p.right);
            }
        }
    }

    @Override
    public int size() {
        return sizeNode(this.root);
    }

    public int sizeNode(Node p) {
        if (p == null) {
            return 0;
        }
        int count = 0;
        count += sizeNode(p.right);
        count++;
        count += sizeNode(p.left);
        return count;
    }

    @Override
    public int leaves() {
        return countNodeLeaves(this.root);
    }
    public int countNodeLeaves(Node node){
        if(node == null) {
            return 0;
        }
        if(node.left == null && node.right == null){
            return 1;
        } else {
            return countNodeLeaves(node.left) + countNodeLeaves(node.right);
        }
    }

    @Override
    public int nodes() {
        return countNodes(this.root);
    }

    public int countNodes(Node node) {
        if (node == null){
            return 0;
        } else {
            return 1 + countNodes(node.left) + countNodes(node.right);
        }
    }

    @Override
    public int height() {
        return getHeight(this.root);
    }

    private int getHeight(Node node) {
        if (node == null){
            return 0;
        } else {
            int lHeight = getHeight(node.left);
            int rHeight = getHeight(node.right);

            return Math.max(lHeight, rHeight) + 1;
        }
    }

    @Override
    public int width() {
        int width;
        int maxWidth = 0;
        int height = height();

        for (int i = 0; i <= height; i++){
            width = getWidth(this.root, i);
            if(width > maxWidth){
                maxWidth = width;
            }
        }
        return maxWidth;
    }

    private int getWidth(Node node, int level) {
        if (node == null){
            return 0;
        }
        if(level == 1){
            return 1;
        } else if (level > 1) {
            return getWidth(node.right, level-1)
                    + getWidth(node.left, level-1);
        } else {
            return 0;
        }
    }

    @Override
    public void reverse() {
        reverseRecursive(this.root);
    }

    private void reverseRecursive(Node node) {
        if (node == null){
            return;
        }
        Node tmp = node.left;
        node.left = node.right;
        node.right = tmp;

        reverseRecursive(node.left);
        reverseRecursive(node.right);
    }

    @Override
    public void delete(int val) {
        this.root = deleteRec(this.root, val);
    }

    public Node deleteRec(Node node, int val) {
        if (node == null){
            return null;
        }
        if(node.value > val){
            node.left = deleteRec(node.left, val);
        } else if (node.value < val){
            node.right = deleteRec(node.right, val);
        } else {
            if (node.right != null && node.left != null){
                Node minRight = minNode(node.right);
                node.value = minRight.value;
                node.right = deleteRec(node.right, minRight.value);
            } else if (node.right != null) {
                node = node.right;
            } else if(node.left != null){
                node = node.left;
            } else {
                node = null;
            }
        }
        return node;
    }

    private Node minNode(Node node) {
        if (node.left == null){
            return node;
        } else {
            return minNode(node.left);
        }
    }

    @Override
    public int[] toArray() {
        if (Objects.isNull(this.root)) {
            return new int[0];
        }
        int[] array = new int[size()];
        Counter c = new Counter();
        toArrayNode(array, c, this.root);
        return array;
    }

    @Override
    public void print() {
        inOrder(this.root);
    }

    public void inOrder(Node node) {
        if (node == null) {
            return;
        }
        inOrder(node.left);
        System.out.printf("%s ", node.value);
        inOrder(node.right);
    }

    public void toArrayNode(int[] array, Counter c, Node node) {
        if (Objects.isNull(node)) {
            return;
        }
        toArrayNode(array, c, node.left);
        array[c.index++] = node.value;
        toArrayNode(array, c, node.right);
    }

    private static class Counter {
        int index = 0;
    }
}

