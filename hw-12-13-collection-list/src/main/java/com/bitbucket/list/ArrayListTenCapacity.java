package com.bitbucket.list;

import com.bitbucket.list.exception.ListEmptyException;
import com.bitbucket.list.exception.PositionOutOfBoundListException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ArrayListTenCapacity implements IList{

    private int[] array = new int[10];

    private int index;

    public int resize(int oldSize){
        int newSize = (int) (oldSize * 1.3);
        this.array = new int[newSize];
        return newSize;
    }

    @Override
    public void init(int[] init) {
        if (init == null) {
            this.index = 0;
        } else {
            this.index = init.length;
            if (this.index > this.array.length) {
                resize(this.index);
            }
            for (int i = 0; i < this.index; i++) {
                this.array[i] = init[i];
            }
        }
    }

    @Override
    public void clear() {
        this.index = 0;
    }

    @Override
    public int size() {
        return this.index;
    }

    @Override
    public int[] toArray() {
        int[] result = new int[this.index];
        for (int i = 0; i < this.index; i++) {
            result[i] = this.array[i];
        }
        return result;
    }

    @Override
    public void addStart(int val) {
        if (this.index == 0) {
            this.array[0] = val;
        }
        int[] tmp = new int[this.index+1];
        for (int i = this.index; i > 0 ; i--) {
            tmp[i] = this.array[i-1];
        }
        tmp[0] = val;
        this.index++;
        this.array = tmp;
    }

    @Override
    public void addEnd(int val) {
        if (this.index == 0) {
            this.array[0] = val;
        }
        int[] tmp = new int[this.index+1];
        for (int i = 0; i < this.index ; i++) {
            tmp[i] = this.array[i];
        }
        tmp[this.index] = val;
        this.index++;
        this.array = tmp;
    }

    @Override
    public void addByPos(int pos, int val) {
        if (this.index == 0) {
            throw new ListEmptyException();
        }
        if (pos < 0 || pos >= this.index) {
            throw new PositionOutOfBoundListException();
        }
        this.index++;
        int[] tmp = new int[this.index];
        for (int i = 0; i < pos; i++) {
            tmp[i] = this.array[i];
        }
        tmp[pos] = val;
        for (int j = pos+1; j < this.index; j++){
            tmp[j] = this.array[j-1];
        }
        this.array = tmp;
    }

    @Override
    public int removeStart() {
        if (this.index == 0) {
            throw new ListEmptyException();
        }
        this.index--;
        int[] tmp = new int[this.index];
        int remove = this.array[0];
        for (int i = 0; i < this.index; i++) {
            tmp[i] = this.array[i+1];
        }
        this.array = tmp;
        return remove;
    }

    @Override
    public int removeEnd() {
        if (this.index == 0) {
            throw new ListEmptyException();
        }
        this.index--;
        int[] tmp = new int[this.index];
        int remove = this.array[this.index];
        for (int i = 0; i < this.index; i++) {
            tmp[i] = this.array[i];
        }
        this.array = tmp;
        return remove;
    }

    @Override
    public int removeByPos(int pos) {
        if (this.index == 0) {
            throw new ListEmptyException();
        }
        if (pos < 0 || pos >= this.index) {
            throw new PositionOutOfBoundListException();
        }
        this.index--;
        int[] tmp = new int[this.index];
        int remove = this.array[pos];
        for (int i = 0; i < this.index; i++) {
            if(pos <= i){
                tmp[i] = this.array[i+1];
            } else {
                tmp[i] = this.array[i];
            }
        }
        this.array = tmp;
        return remove;
    }

    @Override
    public int max() {
        if (this.index == 0) {
            throw new ListEmptyException();
        }
        int max = this.array[0];
        for (int i = 1; i < this.index; i++){
            if (this.array[i] > max){
                max = this.array[i];
            }
        }
        return max;
    }

    @Override
    public int min() {
        if (this.index == 0) {
            throw new ListEmptyException();
        }
        int min = this.array[0];
        for (int i = 1; i < this.index; i++){
            if (this.array[i] < min){
                min = this.array[i];
            }
        }
        return min;
    }

    @Override
    public int maxPos() {
        if (this.index == 0) {
            throw new ListEmptyException();
        }
        int max = this.array[0];
        int index = 0;
        for (int i = 1; i < this.index; i++){
            if (this.array[i] > max){
                max = this.array[i];
                index = i;
            }
        }
        return index;
    }

    @Override
    public int minPos() {
        if (this.index == 0) {
            throw new ListEmptyException();
        }
        int min = this.array[0];
        int index = 0;
        for (int i = 1; i < this.index; i++){
            if (this.array[i] < min){
                min = this.array[i];
                index = i;
            }
        }
        return index;
    }

    @Override
    public int[] sort() {
        if (this.index == 0) {
            throw new ListEmptyException();
        }
        for(int i = 0; i < this.index; i++){
            for(int j = 0; j < this.index-i-1; j++){
                if(this.array[j] > this.array[j+1]){
                    int temp = this.array[j];
                    this.array[j] = this.array[j+1];
                    this.array[j+1] = temp;
                }
            }
        }
        return this.array;
    }

    @Override
    public int get(int pos) {
        if (this.index == 0) {
            throw new ListEmptyException();
        }
        if (pos < 0 || pos >= this.index) {
            throw new PositionOutOfBoundListException();
        }
        return this.array[pos];
    }

    @Override
    public int[] halfRevers() {
        if (this.index == 0) {
            throw new ListEmptyException();
        }
        int[] result = new int[this.index];
        int half;
        if(this.index%2 == 0){
            half = this.index/2;
        } else {
            half = (this.index+1)/2;
        }
        for (int i = half; i < this.index; i++){
            result[i-half] = this.array[i];
        }
        for (int j = 0; j < half; j++){
            result [j+this.index-half] = this.array[j];
        }
        return result;
    }

    @Override
    public int[] revers() {
        if (this.index == 0) {
            throw new ListEmptyException();
        }
        int[] result = new int[this.index];
        for (int i = 0; i < this.index; i++){
            result[i] = this.array[this.index-1-i];
        }
        return result;
    }

    @Override
    public void set(int pos, int val) {
        if (this.index == 0) {
            throw new ListEmptyException();
        }
        if (pos < 0 || pos >= this.index) {
            throw new PositionOutOfBoundListException();
        }
        int[] tmp = new int[this.index];
        for (int i = 0; i < this.index; i++) {
            if (pos == i){
                tmp[i] = val;
            } else {
                tmp[i] = this.array[i];
            }
        }
        this.array = tmp;
    }
}
