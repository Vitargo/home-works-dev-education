<%--
  Created by IntelliJ IDEA.
  User: victory
  Date: 25.04.2021
  Time: 20:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>HoneworkServlet</title>
</head>
<body>
<body>
<%
    //allow access only if session exists

    String userName = null;
    String sessionID = null;
    int status = response.getStatus();
    Cookie[] cookies = request.getCookies();
    if(cookies !=null){
        for(Cookie cookie : cookies){
            if(cookie.getName().equals("user")) userName = cookie.getValue();
            if(cookie.getName().equals("JSESSIONID")) sessionID = cookie.getValue();
        }
    }
%>
<h3>Hi <%=userName %>, Login successful. Your Session ID=<%=sessionID %>. Status <%=status%>!</h3>

</body>
</html>
