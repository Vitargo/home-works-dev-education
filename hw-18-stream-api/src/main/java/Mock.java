import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Mock {

    public static String faculty = "Law faculty";

    public static String group = "A1";

    public static int year = 1988;

    public static int limit = 3;

    public static String toFaculty = "New faculty";

    public static String toGroup = "New group";

    public static String course = "second course";

    public static List<Student> mockStudentsTwo = List.of(
            new Student(1,  "First", "One", 1987, "Kyiv", "0-01-01-1111", "Law faculty", "first course", "A1"),
            new Student(2, "Second", "Two", 1988, "Lviv", "0-01-01-2222", "History faculty", "first course", "B2")
    );

    public static List<Student> mockStudentsOne = List.of(
            new Student(1,  "First", "One", 1987, "Kyiv", "0-01-01-1111", "Law faculty", "first course", "A1")
    );

    public static List<Student> mockStudentIsEmpty = List.of();

    public static List<Student> mock = List.of(

            new Student(1,  "First", "One", 1987, "Kyiv", "0-01-01-1111", "Law faculty", "first course", "A1"),
            new Student(2, "Second", "Two", 1988, "Lviv", "0-01-01-2222", "History faculty", "first course", "B2"),
            new Student(3,"Three","Three",1989,"Kyiv","0-01-01-3333","Math faculty","first course","C4"),
            new Student(4, "Fourth", "Four", 1987, "Rivno", "0-01-01-4444", "Law faculty", "second course", "A1"),
            new Student(5, "Fifth", "Five", 1988, "Kyiv", "0-01-01-5555", "History faculty", "second course", "B3"),
            new Student(6, "Sixth", "Six", 1989, "Lviv", "0-01-01-6666", "Math faculty", "second course", "C3"),
            new Student(7, "Seventh", "Seven", 1992, "Poltava", "0-01-01-7777", "Law faculty", "third course", "A1")
    );

    public static List<Student> mockStudents = new ArrayList<>();

    static {
        mockStudents.add(new Student(1,
                "First",
                "One",
                1987,
                "Kyiv",
                "0-01-01-1111",
                "Law faculty",
                "first course",
                "A1"
        ));
        mockStudents.add(new Student(2,
                "Second",
                "Two",
                1988,
                "Lviv",
                "0-01-01-2222",
                "History faculty",
                "first course",
                "B2"
        ));
        mockStudents.add(new Student(3,
                "Three",
                "Three",
                1989,
                "Kyiv",
                "0-01-01-3333",
                "Math faculty",
                "first course",
                "C4"
        ));
        mockStudents.add(new Student(4,
                "Fourth",
                "Four",
                1987,
                "Rivno",
                "0-01-01-4444",
                "Law faculty",
                "second course",
                "A1"
        ));
        mockStudents.add(new Student(5,
                "Fifth",
                "Five",
                1988,
                "Kyiv",
                "0-01-01-5555",
                "History faculty",
                "second course",
                "B3"
        ));
        mockStudents.add(new Student(6,
                "Sixth",
                "Six",
                1989,
                "Lviv",
                "0-01-01-6666",
                "Math faculty",
                "second course",
                "C3"
        ));
        mockStudents.add(new Student(7,
                "Seventh",
                "Seven",
                1992,
                "Poltava",
                "0-01-01-7777",
                "Law faculty",
                "third course",
                "A1"
        ));
    }
}
