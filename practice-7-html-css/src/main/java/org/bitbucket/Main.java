package org.bitbucket;

import com.github.chat.handlers.WebsocketHandler;
import com.github.chat.network.Broker;
import com.github.chat.network.WebsocketConnectionPool;
import com.github.chat.utils.ServerRunner;
import org.apache.catalina.Context;
import org.apache.catalina.startup.Tomcat;
import org.apache.tomcat.websocket.server.WsContextListener;

import javax.servlet.ServletException;
import javax.websocket.DeploymentException;
import javax.websocket.server.ServerContainer;
import javax.websocket.server.ServerEndpointConfig;
import java.io.File;
import java.util.List;
import java.util.function.Consumer;

public class Main {

    Tomcat tomcat = new Tomcat();

    String webPort = System.getenv("PORT");
        if (webPort == null || webPort.isEmpty()) {
        webPort = "8081";
    }

        tomcat.setPort(Integer.parseInt(webPort));

        tomcat.getHost().setAppBase(".");

    File f = new File("core/web");
    Context ctx = tomcat.addWebapp("", f.getAbsolutePath());
        tomcat.addServlet("","UserHandler",HandlerConfig.usersHandler()).setAsyncSupported(true);
        ctx.addServletMappingDecoded("/users/*", "UserHandler");
        ctx.addApplicationListener(WsContextListener.class.getName());
        this.tomcat.start();
            this.listeners.forEach(lst -> lst.accept(this.ctx));
            this.tomcat.getServer().await();
}
