package org.bitbucket.customserver;

import org.junit.Assert;
import org.junit.Test;

public class MyHttpHandlerTest {

    @Test
    public void getName1(){
        String body = "say=Happy+birthday%21&to=Vita";
        String expect = "Vita";
        String name = MyHttpHandler.getName(body);
        System.out.println(name);
        Assert.assertEquals(expect, name);

    }

    @Test
    public void getGreeting2(){
        String body = "say=Happy+birthday%21&to=Vita";
        String expect = "Happy+birthday%21";
        String name = MyHttpHandler.getGreetings(body);
        System.out.println(name);
        Assert.assertEquals(expect, name);

    }

}