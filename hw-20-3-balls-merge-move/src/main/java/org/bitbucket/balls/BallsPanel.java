package org.bitbucket.balls;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Arrays;

public class BallsPanel extends JPanel {

    private static ArrayList<Ball> balls = new ArrayList<>();

    private static ArrayList<Ball[]> checkCollision = new ArrayList<>();

    private volatile static ArrayList<Ball> closeThread = new ArrayList<>();

    public static ArrayList<Ball> getCloseThread() {
        return closeThread;
    }

    public static void setCounterBalls(int counterBalls) {
        BallsPanel.counterBalls = counterBalls;
    }

    private static int counterBalls = 0;

    public static int getCounterBalls() {
        return counterBalls;
    }

    public BallsPanel() {
        setLayout(null);
        addMouseListener(new MouseListener(){

            @Override
            public void mouseClicked(MouseEvent e) {

            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {
                Ball ball = new Ball(e.getPoint(), BallsPanel.this);
                add(ball);
                System.out.println(ball.toString());
                counterBalls++;
                //System.out.println("Name of new thread: " + counterBalls);
                Thread t = new Thread(ball, "ball-" + counterBalls);
                t.start();
            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }

        });
        setVisible(Boolean.TRUE);
    }

    public synchronized void addPointToList(Ball ball, String threadName) throws InterruptedException {
        balls.add(ball);
        System.out.println("ball added from thread: " + threadName);
        while (balls.size() < BallsPanel.getCounterBalls()){

            try {
                System.out.println(threadName + " waiting! Balls in list = " + balls.size() + ", size of list = " + BallsPanel.getCounterBalls());
                wait();
                System.out.println(threadName + " get up!!");
                System.out.println(threadName );
                return;
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("------" + Arrays.toString(balls.toArray()));
        System.out.println(threadName + " notifyAll! Balls in list = " + balls.size() + ", size of list = " + BallsPanel.getCounterBalls());
        System.out.println(threadName );
        checkCollision = checkCollision();
        //System.out.println(Arrays.toString(checkCollision.toArray()));
        closeThread = resolveCollision(checkCollision);
        //System.out.println(Arrays.toString(closeThread.toArray()));
        notifyAll();
        balls.clear();
    }

    public ArrayList<Ball[]>  checkCollision() {
        int size = balls.size();
        ArrayList<Ball[]> collisionBalls = new ArrayList<>();
        for (int i = 0; i < size; i++){
            for(int j = i+1; j < size; j++){
                double ri = balls.get(i).getRadius()/2;
                double rj = balls.get(j).getRadius()/2;
                double ax = (balls.get(i).getX()+ri) - (balls.get(j).getX()+rj);
                double ay = (balls.get(i).getY()+ri) - (balls.get(j).getY()+rj);
                double a = Math.pow(ax, 2);
                double b = Math.pow(ay, 2);
                double c = a + b;
                double d = Math.pow((ri + rj), 2);
                if (c < d) {
                    collisionBalls.add(new Ball[]{balls.get(i), balls.get(j)});
                    System.out.println(a + " " + b + " " + c + " " +d);
                    System.out.println(balls.get(i).toString() + balls.get(j).toString());
                }
            }
        }
        System.out.println(Arrays.toString(collisionBalls.toArray()));
        return collisionBalls;
    }

    private ArrayList<Ball> resolveCollision(ArrayList<Ball[]> checkCollision) {
        ArrayList<Ball> closeThread = new ArrayList<>();
        Ball ball1;
        Ball ball2;
        for (Ball[] balls : checkCollision) {
            if (balls[0].getRadius() > balls[1].getRadius()) {
                ball1 = balls[0];
                ball2 = balls[1];
            } else {
                ball1 = balls[1];
                ball2 = balls[0];
            }
            ball1.setClr(new Color(ball1.getClr().getRGB() + ball2.getClr().getRGB()));
            int newRadius = ball1.getRadius() + ball2.getRadius();
            ball1.setRadius(Math.min(newRadius, 100));
            ball1.setDy(ball1.getDy() + ball2.getDy());
            ball1.setDx(ball1.getDx() + ball2.getDx());
            System.out.println(ball1);
            closeThread.add(ball2);
        }
        return closeThread;
    }
}
