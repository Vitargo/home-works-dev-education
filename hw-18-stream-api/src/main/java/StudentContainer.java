import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StudentContainer {

    private final List<Student> students = new ArrayList<>();

    public StudentContainer(List<Student> students) {
        init(students);
    }

    private void init(List<Student> students) {
        this.students.addAll(students);
    }

    public List<Student> getStudents(){
        return students;
    }

    //С помощью collect вывести в другую коллекцию следующее:

    //    - список студентов заданного факультета;

    public List<Student> studentsByFaculty(String faculty) {
        return this.students.
                stream().
                filter((p) -> p.getFaculty().equals(faculty)).
                collect(Collectors.toList());
    }

    //    - списки студентов для каждого факультета и курса;

    public List<Student> studentsByFacultyAndCourse(String faculty, String course) {
        return this.students.
                stream().
                filter((p) -> p.getFaculty().equals(faculty)).
                filter((p) -> p.getCourse().equals(course)).
                collect(Collectors.toList());
    }

    //    - список студентов, родившихся после заданного года;

    public List<Student> studentsBirthYear(int year) {
        return this.students.
                stream().
                filter((p) -> p.getYearOfBirth() > year).
                collect(Collectors.toList());
    }

    //    - студента, родившихся после заданного года;

    public Optional<Student> studentByBirthYear(int year) {
        return this.students.
                stream().
                filter((p) -> p.getYearOfBirth() > year).
                findAny();
    }

    //    - список учебной группы в формате Фамилия, Имя, Отчество (List<String>).

    public List<String> studentList(String group) {
        return this.students.
                stream().
                filter((p) -> p.getGroup().equals(group)).
                flatMap(p -> Stream.of(p.toStringForCourse())).
                collect(Collectors.toList());
    }

    //С помощью count:

    //- подсчитать количество студентов на заданном факультете.

    public long studsOnFaculty(String faculty) {
        return this.students.
                stream().
                filter((p) -> p.getFaculty().equals(faculty)).
                count();
    }

    //С помощью map:

    //- перевести студентов на другой факультет.

    public List<Student> newFacultyList(String fromFaculty, String toFaculty) {
        return this.students.
                stream().
                map((p) -> {
                    Student s = new Student(p);
                    if (s.getFaculty().equals(fromFaculty)) {
                        s.setFaculty(toFaculty);
                    }
                    return s;
                }).
                collect(Collectors.toList());
    }

    //- перевести студентов в другую группу.

    public List<Student> newGroupList(String fromGroup, String toGroup) {
        return this.students.
                stream().
                map((p) -> {
                    Student s = new Student(p);
                    if (s.getGroup().equals(fromGroup)) {
                        s.setGroup(toGroup);
                    }
                    return s;
                }).
                collect(Collectors.toList());
    }

    //С помощью find:

    //- подсчитать количество студентов на заданном факультете.

    public int countStudentsWithFind(String faculty) {

        var ref = new Object() {
            int count = 0;
        };
        students.stream().
                filter(s -> s.getFaculty().equals(faculty)).
                forEach(s -> ref.count++);
        return ref.count;
    }

    //С помощью reduce:

    //  - вывести строку в формате Фамилия, Имя, Отчество - Факультет, Группа;
    //разделитель перенос строки, и принимает ограничение по кол-ву студентов;

    public String allStudent(int amountOfStudents) {
        if(amountOfStudents < 0){
            return "Not valid limits!";
        }
        return this.students.
                stream().
                limit(amountOfStudents).
                flatMap((p) -> Stream.of(p.getLastName() + ", " +
                        p.getFirstName() + " - " +
                        p.getFaculty() + ", " +
                        p.getGroup() + ";" + "\n")).
                reduce("", (partialString, element) -> partialString + element);
    }

    //Сгруппировать данные, представив в формате Map<String, List<Student >>:
    //по факультету

    public Map<String, List<Student>> mapFaculty() {
        return this.students.
                stream().
                collect(Collectors.groupingBy(Student::getFaculty));
    }

    //по курсу

    public Map<String, List<Student>> mapCourse() {
        return this.students.
                stream().
                collect(Collectors.groupingBy(Student::getCourse));
    }

    //по группе

    public Map<String, List<Student>> mapGroup() {
        return this.students.
                stream().
                collect(Collectors.groupingBy(Student::getGroup));
    }

    //Написать функцию, которая вернет true/false в зависимости от следующих условий:

    //все студенты учатся на заданном факультете;

    public boolean checkFaculty(String faculty){
        if(this.students.isEmpty()){
            return false;
        }
        return this.students.
                stream().
                allMatch(s->s.getFaculty().equals(faculty));
    }

    //один из студентов учится на заданном факультете;

    public boolean checkOneFaculty(String faculty) {
        if(this.students.isEmpty()){
            return false;
        }
        return this.students.
                stream().
                anyMatch(s -> s.getFaculty().equals(faculty));
    }

    //все студенты учатся в заданной группе;

    public boolean checkGroup(String group) {
        if(this.students.isEmpty()){
            return false;
        }
        return this.students.
                stream().
                allMatch(s -> s.getGroup().equals(group));
    }
    //один из студентов учится в заданной группе

    public boolean checkOneGroup(String group) {
        if(this.students.isEmpty()){
            return false;
        }
        return this.students.
                stream().
                anyMatch(s -> s.getGroup().equals(group));
    }
}
