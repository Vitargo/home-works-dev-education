package org.bitbucket.ws.threadpool;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public class Main {

    public static void main(String[] args) throws IOException {
        ExecutorService executorService = Executors.newFixedThreadPool(10);
        ServerSocket serverSocket = new ServerSocket(8084);
        while(true) {
            Socket socket = serverSocket.accept();
            socket.setSoTimeout(30000);
            executorService.execute(new ConnectionHandler(socket));
        }
    }
}

