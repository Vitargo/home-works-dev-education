package com.bitbucket.bitree;

import java.util.*;

public class BinaryTreeIteration implements ITree{

    private Node root;

    private static class Node {

        int value;

        Node right;

        Node left;

        public Node(int value) {
            this.value = value;
        }
    }

    @Override
    public void init(int[] init) {
        if (Objects.isNull(init)) {
            init = new int[0];
        }
        for (int elem : init) {
            add(elem);
        }
    }

    @Override
    public void clear() {
        this.root = null;
    }

    @Override
    public void add(int val) {
        Node newNode = new Node(val);
        if(this.root == null){
            this.root = newNode;
        } else {
            Node current = this.root;
            Node parent = null;
            while (current != null) {
                parent = current;
                if (val < current.value) {
                    current = current.left;
                } else if (val > current.value) {
                    current = current.right;
                } else {
                    return;
                }
            }
            if (val < parent.value) {
                parent.left = newNode;
            } else {
                parent.right = newNode;
            }
        }
    }

    @Override
    public int size() {
        if (this.root == null)
            return 0;

        Queue<Node> q = new LinkedList<>();
        q.offer(this.root);

        int count = 1;
        while (!q.isEmpty())
        {
            Node tmp = q.poll();
            if (tmp != null)
            {
                if (tmp.left != null)
                {
                    count++;
                    q.offer(tmp.left);
                }
                if (tmp.right != null)
                {
                    count++;
                    q.offer(tmp.right);
                }
            }
        }
        return count;
    }

    @Override
    public int leaves() {
        if(this.root == null){
            return 0;
        }
        int count = 0;
        Queue <Node> queue = new LinkedList<>();
        queue.add(this.root);
        Node tmp;
        while (!queue.isEmpty()){
            tmp = queue.peek();
            queue.poll();
            if (tmp.left != null){
                queue.add(tmp.left);
            }
            if (tmp.right != null){
                queue.add(tmp.right);
            }
            if (tmp.left == null && tmp.right == null){
                count++;
            }
        }
        return count;
    }

    @Override
    public int nodes() {
        if (this.root == null){
            return 0;
        }
        Queue<Node> queue = new LinkedList<>();
        queue.add(this.root);
        int count = 0;
        while(!queue.isEmpty()){
            Node tmp = queue.poll();
            if(tmp != null){
                count++;
            }
            if (tmp.left != null){
                queue.add(tmp.left);
            }
            if (tmp.right != null){
                queue.add(tmp.right);
            }
        }
        return count;
    }

    @Override
    public int height() {
        if (this.root == null){
            return 0;
        }
        Queue<Node> queue = new LinkedList<>();
        queue.add(this.root);
        Node tmp;
        int height = 0;
        while(true){
            int size = queue.size();
            if (size == 0){
                return height;
            }
            height++;
            while(size > 0){
                tmp = queue.poll();
                if (tmp.left != null){
                    queue.add(tmp.left);
                }
                if (tmp.right != null){
                    queue.add(tmp.right);
                }
                size--;
            }
        }
    }

    @Override
    public int width() {
        if (this.root == null){
            return 0;
        }
        int maxwidth = 0;
        Queue<Node> queue = new LinkedList<>();
        queue.add(this.root);
        Node tmp;
        while (!queue.isEmpty()) {
            int count = queue.size();
            maxwidth = Math.max(maxwidth, count);
            while (count-- > 0) {
                tmp = queue.remove();
                if (tmp.left != null) {
                    queue.add(tmp.left);
                }
                if (tmp.right != null) {
                    queue.add(tmp.right);
                }
            }
        }
        return maxwidth;
    }

    @Override
    public void reverse() {
        if (this.root == null){
            return;
        }
        Queue<Node> queue = new LinkedList<>();
        queue.add(this.root);
        Node current;
        Node tmp;
        while(!queue.isEmpty()){
            current = queue.poll();
            tmp = current.left;
            current.left = current.right;
            current.right = tmp;
            if (current.left != null){
                queue.add(current.left);
            }
            if (current.right != null){
                queue.add(current.right);
            }
        }
    }

    @Override
    public void delete(int val) {
        Node current = this.root;
        Node parent = this.root;
        boolean isLeftChild = false;
        while(current.value != val){
            parent = current;
            if(val < current.value){
                current = current.left;
                isLeftChild = true;
            }
            else{
                current = current.right;
                isLeftChild = false;
            }
            if(current == null){
                return;
            }
        }
        if(current.left == null && current.right == null){
            System.out.println("Leaf node deletion case");
            if(current == this.root) {
                this.root = null;
            } else if(isLeftChild){
                parent.left = null;
            } else{
                parent.right = null;
            }
        } else if(current.left == null){
            System.out.println("One right child deletion case");
            if(current == this.root){
                this.root = current.right;
            } else if(isLeftChild){
                parent.left = current.right;
            } else{
                parent.right = current.right;
            }
        } else if(current.right == null){
            System.out.println("One left child deletion case");
            if(current == this.root){
                this.root = current.left;
            } else if(isLeftChild){
                parent.left = current.left;
            } else{
                parent.right = current.left;
            }
        } else{
            System.out.println("Two children deletion case");
            Node successor = findSuccessor(current);
            if(current == this.root){
                this.root = successor;
            } else if(isLeftChild){
                parent.left = successor;
            } else{
                parent.right = successor;
            }
            successor.left = current.left;
        }
    }

    public Node findSuccessor(Node node){
        Node successor = node;
        Node successorParent = node;
        Node current = node.right;
        while(current != null){
            successorParent = successor;
            successor = current;
            current = current.left;
        }
        if(successor != node.right){
            successorParent.left = successor.right;
            successor.right = node.right;
        }
        return successor;
    }

    @Override
    public int[] toArray() {

        if(root == null) {
            return new int[0];
        }
        int[] array = new int[size()];
        int count = 0;

        Stack<Node> stack = new Stack<>();
        Node current = this.root;

        while(!stack.empty() || current != null){
            if(current != null){
                stack.push(current);
                current = current.left;
            } else {
                current = stack.pop();
                array[count] = current.value;
                count ++;
                current = current.right;
            }
        }
        return array;
    }


    @Override
    public void print() {
        if(root == null) {
            System.out.println("Tree is empty!");;
        }
        Stack<Node> stack = new Stack<>();
        Node current = this.root;

        while(!stack.empty() || current != null){
            if(current != null){
                stack.push(current);
                current = current.left;
            } else {
                current = stack.pop();
                System.out.println(current.value + " ");
                current = current.right;
            }
        }
    }
}
