public class Main {

    public static void main(String[] args) {

        StudentContainer sc = new StudentContainer(Mock.mockStudents);

        System.out.printf("List of students on %s:\n" + sc.studentsByFaculty(Mock.faculty) + "\n", Mock.faculty);

        System.out.printf("List of students for %s and %s:\n" + sc.studentsByFacultyAndCourse(Mock.faculty, Mock.course) + "\n", Mock.faculty, Mock.course);

        System.out.printf("List of students born after %d:\n" + sc.studentsBirthYear(Mock.year) +"\n", Mock.year);

        System.out.printf("Student born after %d:\n" + sc.studentByBirthYear(Mock.year) + "\n", Mock.year);

        System.out.printf("List of group %s:\n" + sc.studentList(Mock.group) + "\n", Mock.group);

        System.out.printf("Amount of students on %s: " + sc.studsOnFaculty(Mock.faculty) + "\n", Mock.faculty);

        System.out.println("Success transfer to a new group!\n" + sc.newGroupList(Mock.group, Mock.toGroup));

        System.out.println("Success transfer to a new faculty!\n" + sc.newFacultyList(Mock.faculty, Mock.toFaculty));

        System.out.printf("Amount of students on %s: " + sc.countStudentsWithFind(Mock.faculty) + "\n", Mock.faculty);

        System.out.printf("All students list with limit %d people:\n" + sc.allStudent(Mock.limit), Mock.limit);

        System.out.printf("List of students on %s:\n" + sc.mapFaculty() + "\n", Mock.faculty);

        System.out.printf("List of students on %s:\n" + sc.mapCourse() + "\n", Mock.course);

        System.out.printf("List of students on group %s:\n" + sc.mapGroup() + "\n", Mock.group);

        System.out.println(sc.checkFaculty(Mock.faculty));

        System.out.println(sc.checkGroup(Mock.group));

        System.out.println(sc.checkOneFaculty(Mock.faculty));

        System.out.println(sc.checkOneGroup(Mock.group));
    }
}
