package com.bitbucket.bitree;

public interface ITree {

    void init(int[] init);

    void clear();

    void add(int val);

    int size();

    int leaves();

    int nodes();

    int height();

    int width();

    void reverse();

    void delete(int val);

    String toString();

    int[] toArray();

    void print();
}
