package org.bitbucket.custom.server.nio;

import java.io.IOException;

public class SimpleHttpResponse {

    public SimpleHttpResponse() {
    }

    public static String responseOnGet() {
        String htmlPage = "<!DOCTYPE html>\n" +
                "<html>\n" +
                "<head>\n" +
                "  <meta charset=\"utf-8\">\n" +
                "  <title>Get method example</title>\n" +
                "  <style>\n" +
                "    form {\n" +
                "      width: 420px;\n" +
                "    }\n" +
                "\n" +
                "    div {\n" +
                "      margin-bottom: 20px;\n" +
                "    }\n" +
                "\n" +
                "    label {\n" +
                "      display: inline-block;\n" +
                "      width: 240px;\n" +
                "      text-align: right;\n" +
                "      padding-right: 10px;\n" +
                "    }\n" +
                "\n" +
                "    button, input {\n" +
                "      float: right;\n" +
                "    }\n" +
                "  </style>\n" +
                "</head>\n" +
                "<body>\n" +
                "<form action=\"http://localhost:8083\" method=\"post\">\n" +
                "  <div>\n" +
                "    <label for=\"say\">What greeting do you want to say?</label>\n" +
                "    <input name=\"say\" id=\"say\" value=\"Hi\">\n" +
                "  </div>\n" +
                "  <div>\n" +
                "    <label for=\"to\">Who do you want to say it to?</label>\n" +
                "    <input name=\"to\" value=\"Mom\">\n" +
                "  </div>\n" +
                "  <div>\n" +
                "    <button>Send my greetings</button>\n" +
                "  </div>\n" +
                "</form>\n" +
                "</body>\n" +
                "</html>";

        return "HTTP/1.1 200 OK\n\r" +
                "Content-Length: " + (htmlPage.length() + 1) + "\n\r" +
                "Content-Type: text/html; charset=utf-8\n\r\n\r" + htmlPage + "\n\r\n\r";
    }

    public static String responseOnPost(String name, String greetings) throws IOException {
        String htmlPage = "<!DOCTYPE html>\n" +
                "<html>\n" +
                "<head>\n" +
                "  <meta charset=\"utf-8\">\n" +
                "  <title>Greeting</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "<p>" + name + "! " + greetings + "</p>\n" +
                "</body>\n" +
                "</html>";

        return "HTTP/1.1 200 OK\n\r" +
                "Content-Length: " + (htmlPage.length() + 1) + "\n\r" +
                "Content-Type: text/html; charset=utf-8\n\r\n\r" + htmlPage + "\n\r\n\r";
    }

    public static String responseOnPut() {
        String htmlPage = "<!DOCTYPE html>\n" +
                "<html>\n" +
                "<head>\n" +
                "  <meta charset=\"utf-8\">\n" +
                "  <title>Greeting</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "<p>Anonymous! Hello!</p>\n" +
                "</body>\n" +
                "</html>";

        return "HTTP/1.1 200 OK\n\r" +
                "Content-Length: " + (htmlPage.length() + 1) + "\n\r" +
                "Content-Type: text/html; charset=utf-8\n\r\n\r" + htmlPage + "\n\r\n\r";
    }

    public static String responseOnDelete() {
        String htmlPage = "<!DOCTYPE html>\n" +
                "<html>\n" +
                "<head>\n" +
                "  <meta charset=\"utf-8\">\n" +
                "  <title>Greeting</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "<p>Delete All Greetings!!!</p>\n" +
                "</body>\n" +
                "</html>";

        return "HTTP/1.1 200 OK\n\r" +
                "Content-Length: " + (htmlPage.length() + 1) + "\n\r" +
                "Content-Type: text/html; charset=utf-8\n\r\n\r" + htmlPage + "\n\r\n\r";
    }

    public static String responseOnOther() {
        String htmlPage = "<!DOCTYPE html>\n" +
                "<html>\n" +
                "<head>\n" +
                "  <meta charset=\"utf-8\">\n" +
                "  <title>Greeting</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "<p>Not valid HTTP request!</p>\n" +
                "</body>\n" +
                "</html>";

        return "HTTP/1.1 200 OK\n\r" +
                "Content-Length: " + (htmlPage.length() + 1) + "\n\r" +
                "Content-Type: text/html; charset=utf-8\n\r\n\r" + htmlPage + "\n\r\n\r";
    }
}
