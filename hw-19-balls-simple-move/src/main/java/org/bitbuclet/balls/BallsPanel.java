package org.bitbuclet.balls;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class BallsPanel extends JPanel {

    private static int counterBalls = 0;

    public BallsPanel() {
        setLayout(null);
        addMouseListener(new MouseListener(){

            @Override
            public void mouseClicked(MouseEvent e) {

            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {
                Point point = e.getPoint();
                if(point.x + 70 >= getWidth()){
                    point.x -= 70;
                }
                if(point.y + 70 >= getHeight()){
                    point.y -= 70;
                }
                Ball ball = new Ball(point);
                add(ball);
                counterBalls++;
                Thread t = new Thread(ball, "ball-" + counterBalls);
                t.start();
            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });
        setVisible(Boolean.TRUE);
    }
}
