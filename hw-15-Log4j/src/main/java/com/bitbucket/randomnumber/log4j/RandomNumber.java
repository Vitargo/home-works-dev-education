package com.bitbucket.randomnumber.log4j;

import org.apache.log4j.Logger;

import java.util.Random;

public class RandomNumber {

    private static final Logger log = Logger.getLogger(RandomNumber.class);

    public static int oneRandomNun(){
        int randomNumber= 0;
        try{
            Random random = new Random();
            randomNumber = random.nextInt(11);
            if(randomNumber <= 5) {
                throw new CustomException("Generated number - " + randomNumber);
            } else {
                log.info("App was successfully done!");
            }
        } catch (CustomException exception) {
            log.error(exception.getMessage());
        }
        return randomNumber;
    }
}
