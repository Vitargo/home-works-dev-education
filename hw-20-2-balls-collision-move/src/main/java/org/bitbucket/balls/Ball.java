package org.bitbucket.balls;

import javax.swing.*;
import java.awt.*;
import java.util.Objects;
import java.util.Random;

public class Ball extends JPanel implements Runnable {

    private final Random rnd = new Random();

    private int dx;

    private int dy;

    private final Color clr;

    private int radius;

    private Point point;

    private BallsPanel panel;

    public Ball(Point point, BallsPanel panel) {
        this.point = point;
        this.clr = randomColor();
        this.radius = randomRadius();
        this.dx = this.rnd.nextInt(5)-3;
        this.dy = this.rnd.nextInt (5)-3;
        this.panel = panel;
        setSize(radius,radius);
        setOpaque(Boolean.FALSE);
    }

    private void move() {
        JPanel pan = (JPanel) getParent();
        panel.addPointToList(this, Thread.currentThread().getName());
        if (this.point.x <= 0 || this.point.x + radius >= pan.getWidth()) {
            dx = -dx;
        }
        if (this.point.y <= 0 || this.point.y + radius >= pan.getHeight()) {
            dy = -dy;
        }
        this.point.translate(dx, dy);
        setLocation(point);
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2d = (Graphics2D) g;
        g2d.setColor(clr);
        g2d.fillOval(0,0,radius,radius);
    }

    @Override
    public void run() {
        try {
            while (true) {
                move();
                Thread.sleep(10);

            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupted();
        }
    }

    public Color randomColor(){
        int r,g,b;
        r = (int)(255 * Math.random());
        g = (int)(255 * Math.random());
        b = (int)(255 * Math.random());
        Color c = new Color(r,g,b);
        return c;
    }

    public int randomRadius(){
        Random random = new Random();
        return random.nextInt(50 - 10 + 1) + 10;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ball ball = (Ball) o;
        return dx == ball.dx && dy == ball.dy && radius == ball.radius && Objects.equals(rnd, ball.rnd) && Objects.equals(clr, ball.clr) && Objects.equals(point, ball.point) && Objects.equals(panel, ball.panel);
    }

    @Override
    public int hashCode() {
        return Objects.hash(rnd, dx, dy, clr, radius, point, panel);
    }

    @Override
    public String toString() {
        return "Ball{" +
                "rnd=" + rnd +
                ", dx=" + dx +
                ", dy=" + dy +
                ", clr=" + clr +
                ", radius=" + radius +
                ", point=" + point +
                ", panel=" + panel +
                '}';
    }

    public int getRadius() {
        return radius;
    }

    public int getX() {
        return point.x;
    }

    public int getY() {
        return point.y;
    }

    public int getDx() {
        return dx;
    }

    public int getDy() {
        return dy;
    }

    public void setDx(int dx) {
        this.dx = dx;
    }

    public void setDy(int dy) {
        this.dy = dy;
    }

    public void setX(int x) {
        this.point.x = x;
    }

    public void setY(int y) {
        this.point.y = y;
    }

    public void setPoint( int dx, int dy){
        this.point.translate(dx, dy);
    }
}

