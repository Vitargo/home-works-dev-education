package org.bitbucket.servlet;

import org.apache.catalina.LifecycleException;
import org.apache.catalina.startup.Tomcat;

import javax.servlet.ServletException;

public class Main {
    public static void main(String[] args) throws ServletException, LifecycleException {
        Tomcat tomcat = new Tomcat();
        String webPort = System.getenv("PORT");
        if(webPort == null || webPort.isEmpty()){
            webPort = "8081";
        }
        tomcat.setPort(Integer.parseInt(webPort));
        tomcat.addWebapp("", "/Users/victory/Documents/workspace/deveducation/home-works-dev-education/hw-23-homework-servlet/web");
        tomcat.start();
        tomcat.getServer().await();
    }
}
