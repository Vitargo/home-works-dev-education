import org.junit.Assert;
import org.junit.Test;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class StudentContainerTest {

    StudentContainer sc = new StudentContainer(Mock.mockStudents);
    StudentContainer scTwo = new StudentContainer(Mock.mockStudentsTwo);
    StudentContainer scOne = new StudentContainer(Mock.mockStudentsOne);
    StudentContainer scIsEmpty = new StudentContainer((Mock.mockStudentIsEmpty));

    //====================================================
    //================studentsByFaculty===================
    //====================================================

    @Test
    public void studentsByFacultyMany (){
        List<Student> exp = List.of(
                new Student(1, "First", "One", 1987, "Kyiv", "0-01-01-1111", "Law faculty", "first course", "A1"),
                new Student(4, "Fourth", "Four", 1987, "Rivno", "0-01-01-4444", "Law faculty", "second course", "A1"),
                new Student(7, "Seventh", "Seven", 1992, "Poltava", "0-01-01-7777", "Law faculty", "third course", "A1")
        );
        List<Student> act = sc.studentsByFaculty(Mock.faculty);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void studentsByFacultyTwo(){
        List<Student> exp = List.of(
                new Student(1, "First", "One", 1987, "Kyiv", "0-01-01-1111", "Law faculty", "first course", "A1")
        );
        List<Student> act = scTwo.studentsByFaculty(Mock.faculty);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void studentsByFacultyOne(){
        List<Student> exp = List.of(
                new Student(1, "First", "One", 1987, "Kyiv", "0-01-01-1111", "Law faculty", "first course", "A1")
        );
        List<Student> act = scOne.studentsByFaculty(Mock.faculty);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void studentsByFacultyOne2(){
        List<Student> exp = List.of();
        List<Student> act = scOne.studentsByFaculty("History faculty");
        Assert.assertEquals(exp, act);
    }

    @Test
    public void studentsByFacultyEmpty(){
        List<Student> exp = List.of();
        List<Student> act = scIsEmpty.studentsByFaculty(Mock.faculty);
        Assert.assertEquals(exp, act);
    }

    //====================================================
    //============studentsByFacultyAndCourse==============
    //====================================================

    @Test
    public void studentsByFacultyAndCourseMany (){
        List<Student> exp = List.of(
                new Student(4, "Fourth", "Four", 1987, "Rivno", "0-01-01-4444", "Law faculty", "second course", "A1")
        );
        List<Student> act = sc.studentsByFacultyAndCourse(Mock.faculty, Mock.course);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void studentsByFacultyAndCourseTwo(){
        List<Student> exp = List.of(
                new Student(1,  "First", "One", 1987, "Kyiv", "0-01-01-1111", "Law faculty", "first course", "A1")
        );
        List<Student> act = scTwo.studentsByFacultyAndCourse(Mock.faculty, "first course");
        Assert.assertEquals(exp, act);
    }

    @Test
    public void studentsByFacultyAndCourseOne(){
        List<Student> exp = List.of(
                new Student(1,  "First", "One", 1987, "Kyiv", "0-01-01-1111", "Law faculty", "first course", "A1")
        );
        List<Student> act = scOne.studentsByFacultyAndCourse(Mock.faculty, "first course");
        Assert.assertEquals(exp, act);
    }

    @Test
    public void studentsByFacultyAndCourseOne2(){
        List<Student> exp = List.of();
        List<Student> act = scTwo.studentsByFacultyAndCourse(Mock.faculty, Mock.course);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void studentsByFacultyAndCourseEmpty(){
        List<Student> exp = List.of();
        List<Student> act = scIsEmpty.studentsByFacultyAndCourse(Mock.faculty, Mock.course);
        Assert.assertEquals(exp, act);
    }

    //====================================================
    //============studentsBirthYear=======================
    //====================================================

    @Test
    public void studentsBirthYearMany(){
        List<Student> exp = List.of(
                new Student(7, "Seventh", "Seven", 1992, "Poltava", "0-01-01-7777", "Law faculty", "third course", "A1")
        );
        List<Student> act = sc.studentsBirthYear(1990);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void studentsBirthYearMany2(){
        List<Student> exp = List.of(
                new Student(3,"Three","Three",1989,"Kyiv","0-01-01-3333","Math faculty","first course","C4"),
                new Student(6, "Sixth", "Six", 1989, "Lviv", "0-01-01-6666", "Math faculty", "second course", "C3"),
                new Student(7, "Seventh", "Seven", 1992, "Poltava", "0-01-01-7777", "Law faculty", "third course", "A1")
        );
        List<Student> act = sc.studentsBirthYear(Mock.year);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void studentsBirthYearTwo(){
        List<Student> exp = List.of(
                new Student(2, "Second", "Two", 1988, "Lviv", "0-01-01-2222", "History faculty", "first course", "B2")
        );
        List<Student> act = scTwo.studentsBirthYear(1987);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void studentsBirthYearOne(){
        List<Student> exp = List.of(
                new Student(1,  "First", "One", 1987, "Kyiv", "0-01-01-1111", "Law faculty", "first course", "A1")
        );
        List<Student> act = scOne.studentsBirthYear(1986);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void studentsBirthYearEmpty(){
        List<Student> exp = List.of();
        List<Student> act = scIsEmpty.studentsBirthYear(1986);
        Assert.assertEquals(exp, act);
    }

    //====================================================
    //============studentList=============================
    //====================================================

    @Test
    public void studentListMany(){
        List<String> exp = List.of("First One", "Fourth Four", "Seventh Seven");
        List<String> act = sc.studentList(Mock.group);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void studentListTwo(){
        List<String> exp = List.of("First One");
        List<String> act = scTwo.studentList(Mock.group);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void studentListOne(){
        List<String> exp = List.of("First One");
        List<String> act = scOne.studentList(Mock.group);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void studentListEmpty(){
        List<String> exp = List.of();
        List<String> act = scIsEmpty.studentList(Mock.group);
        Assert.assertEquals(exp, act);
    }

    //====================================================
    //============studsOnFaculty=============================
    //====================================================

    @Test
    public void studsOnFacultyMany(){
        long exp = 3;
        long act = sc.studsOnFaculty(Mock.faculty);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void studsOnFacultyTwo(){
        long exp = 1;
        long act = scOne.studsOnFaculty(Mock.faculty);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void studsOnFacultyOne(){
        long exp = 1;
        long act = scOne.studsOnFaculty(Mock.faculty);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void studsOnFacultyEmpty(){
        long exp = 0;
        long act = scIsEmpty.studsOnFaculty(Mock.faculty);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void studsOnFacultyNotExist(){
        long exp = 0;
        long act = sc.studsOnFaculty("New faculty");
        Assert.assertEquals(exp, act);
    }

    //====================================================
    //============newFacultyList=======================
    //====================================================

    @Test
    public void newFacultyListMane(){
        List<Student> exp = List.of(
                new Student(1,  "First", "One", 1987, "Kyiv", "0-01-01-1111", "New faculty", "first course", "A1"),
                new Student(2, "Second", "Two", 1988, "Lviv", "0-01-01-2222", "History faculty", "first course", "B2"),
                new Student(3,"Three","Three",1989,"Kyiv","0-01-01-3333","Math faculty","first course","C4"),
                new Student(4, "Fourth", "Four", 1987, "Rivno", "0-01-01-4444", "New faculty", "second course", "A1"),
                new Student(5, "Fifth", "Five", 1988, "Kyiv", "0-01-01-5555", "History faculty", "second course", "B3"),
                new Student(6, "Sixth", "Six", 1989, "Lviv", "0-01-01-6666", "Math faculty", "second course", "C3"),
                new Student(7, "Seventh", "Seven", 1992, "Poltava", "0-01-01-7777", "New faculty", "third course", "A1")
        );
        List<Student> act = sc.newFacultyList(Mock.faculty, Mock.toFaculty);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void newFacultyListTwo(){
        List<Student> exp = List.of(
                new Student(1,  "First", "One", 1987, "Kyiv", "0-01-01-1111", "New faculty", "first course", "A1"),
                new Student(2, "Second", "Two", 1988, "Lviv", "0-01-01-2222", "History faculty", "first course", "B2")
                );
        List<Student> act = scTwo.newFacultyList(Mock.faculty, Mock.toFaculty);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void newFacultyListOne(){
        List<Student> exp = List.of(
                new Student(1,  "First", "One", 1987, "Kyiv", "0-01-01-1111", "New faculty", "first course", "A1")
        );
        List<Student> act = scOne.newFacultyList(Mock.faculty, Mock.toFaculty);
        Assert.assertEquals(exp, act);
    }
    @Test
    public void newFacultyListEmpty(){
        List<Student> exp = List.of();
        List<Student> act = scIsEmpty.newFacultyList(Mock.faculty, Mock.toFaculty);
        Assert.assertEquals(exp, act);
    }

    //====================================================
    //============newGroupList============================
    //====================================================

    @Test
    public void newGroupListMany(){
        List<Student> exp = List.of(
                new Student(1,  "First", "One", 1987, "Kyiv", "0-01-01-1111", "Law faculty", "first course", "New group"),
                new Student(2, "Second", "Two", 1988, "Lviv", "0-01-01-2222", "History faculty", "first course", "B2"),
                new Student(3,"Three","Three",1989,"Kyiv","0-01-01-3333","Math faculty","first course","C4"),
                new Student(4, "Fourth", "Four", 1987, "Rivno", "0-01-01-4444", "Law faculty", "second course", "New group"),
                new Student(5, "Fifth", "Five", 1988, "Kyiv", "0-01-01-5555", "History faculty", "second course", "B3"),
                new Student(6, "Sixth", "Six", 1989, "Lviv", "0-01-01-6666", "Math faculty", "second course", "C3"),
                new Student(7, "Seventh", "Seven", 1992, "Poltava", "0-01-01-7777", "Law faculty", "third course", "New group")
        );
        List<Student> act = sc.newGroupList(Mock.group, Mock.toGroup);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void newGroupListTwo(){
        List<Student> exp = List.of(
                new Student(1,  "First", "One", 1987, "Kyiv", "0-01-01-1111", "Law faculty", "first course", "New group"),
                new Student(2, "Second", "Two", 1988, "Lviv", "0-01-01-2222", "History faculty", "first course", "B2")
        );
        List<Student> act = scTwo.newGroupList(Mock.group, Mock.toGroup);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void newGroupListOne(){
        List<Student> exp = List.of(
                new Student(1,  "First", "One", 1987, "Kyiv", "0-01-01-1111", "Law faculty", "first course", "New group")
        );
        List<Student> act = scOne.newGroupList(Mock.group, Mock.toGroup);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void newGroupListEmpty(){
        List<Student> exp = List.of();
        List<Student> act = scIsEmpty.newGroupList(Mock.group, Mock.toGroup);
        Assert.assertEquals(exp, act);
    }

    //====================================================
    //============countStudentsWithFind===================
    //====================================================

    @Test
    public void countStudentsWithFindMany(){
        int exp = 3;
        int act = sc.countStudentsWithFind(Mock.faculty);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void countStudentsWithFindTwo(){
        int exp = 1;
        int act = scOne.countStudentsWithFind(Mock.faculty);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void countStudentsWithFindOne(){
        int exp = 1;
        int act = scOne.countStudentsWithFind(Mock.faculty);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void countStudentsWithFindEmpty(){
        int exp = 0;
        int act = scIsEmpty.countStudentsWithFind(Mock.faculty);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void countStudentsWithFindNotExist(){
        int exp = 0;
        int act = sc.countStudentsWithFind("New faculty");
        Assert.assertEquals(exp, act);
    }

    //====================================================
    //============allStudent==============================
    //====================================================

    @Test
    public void allStudentMany(){
        String exp = "One, First - Law faculty, A1;\n" +
                "Two, Second - History faculty, B2;\n" +
                "Three, Three - Math faculty, C4;\n";
        String act = sc.allStudent(Mock.limit);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void allStudentTwo(){
        String exp = "One, First - Law faculty, A1;\n" +
                "Two, Second - History faculty, B2;\n";
        String act = scTwo.allStudent(Mock.limit);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void allStudentOne(){
        String exp = "One, First - Law faculty, A1;\n";
        String act = scOne.allStudent(Mock.limit);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void allStudentEmpty(){
        String exp = "";
        String act = scIsEmpty.allStudent(Mock.limit);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void allStudentZero() {
        String exp = "";
        String act = sc.allStudent(0);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void allStudentNotValid() {
        String exp = "Not valid limits!";
        String act = sc.allStudent(-1);
        Assert.assertEquals(exp, act);
    }

    //====================================================
    //============mapFaculty==============================
    //====================================================

    @Test
    public void mapFacultyMany(){
        Map<String, List<Student>> exp = Map.of("Math faculty", List.of(
                new Student(3,"Three","Three",1989,"Kyiv","0-01-01-3333","Math faculty","first course","C4"),
                new Student(6, "Sixth", "Six", 1989, "Lviv", "0-01-01-6666", "Math faculty", "second course", "C3")
                ), "History faculty", List.of(
                new Student(2, "Second", "Two", 1988, "Lviv", "0-01-01-2222", "History faculty", "first course", "B2"),
                new Student(5, "Fifth", "Five", 1988, "Kyiv", "0-01-01-5555", "History faculty", "second course", "B3")
                ), "Law faculty", List.of(
                new Student(1,  "First", "One", 1987, "Kyiv", "0-01-01-1111", "Law faculty", "first course", "A1"),
                new Student(4, "Fourth", "Four", 1987, "Rivno", "0-01-01-4444", "Law faculty", "second course", "A1"),
                new Student(7, "Seventh", "Seven", 1992, "Poltava", "0-01-01-7777", "Law faculty", "third course", "A1")
        ));
        Map<String, List<Student>>act = sc.mapFaculty();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void mapFacultyTwo(){
        Map<String, List<Student>> exp = Map.of("History faculty", List.of(
                new Student(2, "Second", "Two", 1988, "Lviv", "0-01-01-2222", "History faculty", "first course", "B2")),
                "Law faculty", List.of(
                new Student(1,  "First", "One", 1987, "Kyiv", "0-01-01-1111", "Law faculty", "first course", "A1")
                ));
        Map<String, List<Student>>act = scTwo.mapFaculty();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void mapFacultyOne(){
        Map<String, List<Student>> exp = Map.of("Law faculty", List.of(
                        new Student(1,  "First", "One", 1987, "Kyiv", "0-01-01-1111", "Law faculty", "first course", "A1")
                ));
        Map<String, List<Student>>act = scOne.mapFaculty();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void mapFacultyEmpty(){
        Map<String, List<Student>> exp = Map.of();
        Map<String, List<Student>>act = scIsEmpty.mapFaculty();
        Assert.assertEquals(exp, act);
    }

    //====================================================
    //============mapCourse==============================
    //====================================================

    @Test
    public void mapCourseMany(){
        Map<String, List<Student>> exp = Map.of("first course", List.of(
                new Student(1,  "First", "One", 1987, "Kyiv", "0-01-01-1111", "Law faculty", "first course", "A1"),
                new Student(2, "Second", "Two", 1988, "Lviv", "0-01-01-2222", "History faculty", "first course", "B2"),
                new Student(3,"Three","Three",1989,"Kyiv","0-01-01-3333","Math faculty","first course","C4")

        ), "third course", List.of(
                new Student(7, "Seventh", "Seven", 1992, "Poltava", "0-01-01-7777", "Law faculty", "third course", "A1")
        ), "second course", List.of(
                new Student(4, "Fourth", "Four", 1987, "Rivno", "0-01-01-4444", "Law faculty", "second course", "A1"),
                new Student(5, "Fifth", "Five", 1988, "Kyiv", "0-01-01-5555", "History faculty", "second course", "B3"),
                new Student(6, "Sixth", "Six", 1989, "Lviv", "0-01-01-6666", "Math faculty", "second course", "C3")

        ));
        Map<String, List<Student>>act = sc.mapCourse();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void mapCourseTwo(){
        Map<String, List<Student>> exp = Map.of("first course", List.of(
                new Student(1,  "First", "One", 1987, "Kyiv", "0-01-01-1111", "Law faculty", "first course", "A1"),
                new Student(2, "Second", "Two", 1988, "Lviv", "0-01-01-2222", "History faculty", "first course", "B2")
        ));
        Map<String, List<Student>>act = scTwo.mapCourse();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void mapCourseOne(){
        Map<String, List<Student>> exp = Map.of("first course", List.of(
                new Student(1,  "First", "One", 1987, "Kyiv", "0-01-01-1111", "Law faculty", "first course", "A1")
        ));
        Map<String, List<Student>>act = scOne.mapCourse();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void mapCourseEmpty(){
        Map<String, List<Student>> exp = Map.of();
        Map<String, List<Student>>act = scIsEmpty.mapCourse();
        Assert.assertEquals(exp, act);
    }

    //====================================================
    //============mapGroup================================
    //====================================================

    @Test
    public void mapGroupMany(){
        Map<String, List<Student>> exp = Map.of("C4", List.of(
                new Student(3,"Three","Three",1989,"Kyiv","0-01-01-3333","Math faculty","first course","C4")
        ),"C3", List.of(
                new Student(6, "Sixth", "Six", 1989, "Lviv", "0-01-01-6666", "Math faculty", "second course", "C3")
        ), "B2", List.of(
                new Student(2, "Second", "Two", 1988, "Lviv", "0-01-01-2222", "History faculty", "first course", "B2")
        ), "B3", List.of(
                new Student(5, "Fifth", "Five", 1988, "Kyiv", "0-01-01-5555", "History faculty", "second course", "B3")
        ),"A1", List.of(
                new Student(1,  "First", "One", 1987, "Kyiv", "0-01-01-1111", "Law faculty", "first course", "A1"),
                new Student(4, "Fourth", "Four", 1987, "Rivno", "0-01-01-4444", "Law faculty", "second course", "A1"),
                new Student(7, "Seventh", "Seven", 1992, "Poltava", "0-01-01-7777", "Law faculty", "third course", "A1")
        ));
        Map<String, List<Student>>act = sc.mapGroup();
        Assert.assertEquals(exp, act);
    }
    @Test
    public void mapGroupTwo(){
        Map<String, List<Student>> exp = Map.of("B2", List.of(
                new Student(2, "Second", "Two", 1988, "Lviv", "0-01-01-2222", "History faculty", "first course", "B2")
        ), "A1", List.of(
                new Student(1,  "First", "One", 1987, "Kyiv", "0-01-01-1111", "Law faculty", "first course", "A1")
        ));
        Map<String, List<Student>>act = scTwo.mapGroup();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void mapGroupOne(){
        Map<String, List<Student>> exp = Map.of("A1", List.of(
                new Student(1,  "First", "One", 1987, "Kyiv", "0-01-01-1111", "Law faculty", "first course", "A1")
        ));
        Map<String, List<Student>>act = scOne.mapGroup();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void mapGroupEmpty(){
        Map<String, List<Student>> exp = Map.of();
        Map<String, List<Student>>act = scIsEmpty.mapGroup();
        Assert.assertEquals(exp, act);
    }

    //====================================================
    //============checkFaculty============================
    //====================================================

    @Test
    public void checkFacultyMany(){
        boolean act = sc.checkFaculty(Mock.faculty);
        assertFalse(act);
    }

    @Test
    public void checkFacultyTwo(){
        boolean act = scTwo.checkFaculty(Mock.faculty);
        assertFalse(act);
    }

    @Test
    public void checkFacultyOne(){
        boolean act = scOne.checkFaculty(Mock.faculty);
        assertTrue(act);
    }

    @Test
    public void checkFacultyEmpty(){
        boolean act = scIsEmpty.checkFaculty(Mock.faculty);
        assertFalse(act);
    }

    //====================================================
    //============checkOneFaculty=========================
    //====================================================

    @Test
    public void checkOneFacultyMany(){
        boolean act = sc.checkOneFaculty(Mock.faculty);
        assertTrue(act);
    }

    @Test
    public void checkOneFacultyTwo(){
        boolean act = scTwo.checkOneFaculty(Mock.faculty);
        assertTrue(act);
    }

    @Test
    public void checkOneFacultyOne(){
        boolean act = scOne.checkOneFaculty(Mock.faculty);
        assertTrue(act);
    }

    @Test
    public void checkOneFacultyEmpty(){
        boolean act = scIsEmpty.checkFaculty(Mock.faculty);
        assertFalse(act);
    }

    //====================================================
    //============checkGroup==============================
    //====================================================

    @Test
    public void checkGroupMany(){
        boolean act = sc.checkGroup(Mock.group);
        assertFalse(act);
    }

    @Test
    public void checkGroupTwo(){
        boolean act = scTwo.checkGroup(Mock.group);
        assertFalse(act);
    }

    @Test
    public void checkGroupOne(){
        boolean act = scOne.checkGroup(Mock.group);
        assertTrue(act);
    }

    @Test
    public void checkGroupEmpty(){
        boolean act = scIsEmpty.checkGroup(Mock.group);
        assertFalse(act);
    }

    //====================================================
    //============checkOneGroup===========================
    //====================================================

    @Test
    public void checkOneGroupMany(){
        boolean act = sc.checkOneGroup(Mock.group);
        assertTrue(act);
    }

    @Test
    public void checkOneGroupTwo(){
        boolean act = scTwo.checkOneGroup(Mock.group);
        assertTrue(act);
    }

    @Test
    public void checkOneGroupOne(){
        boolean act = scOne.checkOneGroup(Mock.group);
        assertTrue(act);
    }

    @Test
    public void checkOneGroupEmpty(){
        boolean act = scIsEmpty.checkOneGroup(Mock.group);
        assertFalse(act);
    }
}