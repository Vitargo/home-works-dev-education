package com.bitbucket.bitree;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class BinaryTreeRecursionTest {

    private final String name;

    private final ITree tree;

    public BinaryTreeRecursionTest(String name, ITree tree) {
        this.name = name;
        this.tree = tree;
    }

    @Before
    public void setUp() {
        this.tree.clear();
    }

    @Parameterized.Parameters(name = "{index} {0}")
    public static Collection<Object[]> instances() {
        return Arrays.asList(new Object[][] {
                {"Binary Tree Recursion", new BinaryTreeRecursion()},
                {"Binary Tree Iteration", new BinaryTreeIteration()}
        });
    }

    //=================================================
    //=================== Clean =======================
    //=================================================

    @Test
    public void clearMany() {
        int[] array = new int[]{1, 232, 43432, 123, 543, 4343, 123, 5644, 34, 12};
        this.tree.init(array);
        this.tree.clear();
        int[] exp = {};
        int[] act = this.tree.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void clearMany2() {
        int[] array = new int[]{1, 2, 3, 7, 900};
        this.tree.init(array);
        this.tree.clear();
        int[] exp = {};
        int[] act = this.tree.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void clearTwo() {
        int[] array = new int[]{1, 232};
        this.tree.init(array);
        this.tree.clear();
        int[] exp = {};
        int[] act = this.tree.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void clearOne() {
        int[] array = new int[]{1};
        this.tree.init(array);
        this.tree.clear();
        int[] exp = {};
        int[] act = this.tree.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void clearEmpty() {
        int[] array = new int[]{};
        this.tree.init(array);
        this.tree.clear();
        int[] exp = {};
        int[] act = this.tree.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    //=================================================
    //=================== Size ========================
    //=================================================

    @Test
    public void sizeMany() {
        int[] array = new int[]{1, 232, 43432, 123, 543, 4343, 125, 5644, 34, 12};
        this.tree.init(array);
        int exp = 10;
        int act = this.tree.size();
        this.tree.clear();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void sizeMany2() {
        int[] array = new int[]{-8, 1, 232, 43432, -123, 543, 4343, 125, 5644, 34, 12};
        this.tree.init(array);
        int exp = 11;
        int act = this.tree.size();
        this.tree.clear();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void Two() {
        int[] array = new int[]{-8, 1};
        this.tree.init(array);
        int exp = 2;
        int act = this.tree.size();
        this.tree.clear();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void sizeOne() {
        int[] array = new int[]{-8};
        this.tree.init(array);
        int exp = 1;
        int act = this.tree.size();
        this.tree.clear();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void sizeEmpty() {
        int[] array = new int[]{};
        this.tree.init(array);
        int exp = 0;
        int act = this.tree.size();
        this.tree.clear();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void sizeTheSame() {
        int[] array = new int[]{1, 3, 3, 232, 43432, 123, 543, 4343, 123, 5644, 34, 12};
        this.tree.init(array);
        int exp = 10;
        int act = this.tree.size();
        this.tree.clear();
        Assert.assertEquals(exp, act);
    }

    //=================================================
    //=================== Add =========================
    //=================================================

    @Test
    public void addMany() {
        int[] array = new int[]{232, 43432, -123, 543, 4343, 123, 5644, 34, 12, 44};
        this.tree.init(array);
        int[] exp = {-123, 1, 12, 34, 44, 123, 232, 543, 4343, 5644, 43432};
        this.tree.add(1);
        int[] act = this.tree.toArray();
        this.tree.clear();
        System.out.println(Arrays.toString(act));
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void addFive() {
        int[] array = new int[]{232, 43432, -123, 543, 4343};
        this.tree.init(array);
        int[] exp = {-123, 0, 232, 543, 4343, 43432};
        this.tree.add(0);
        int[] act = this.tree.toArray();
        this.tree.clear();
        System.out.println(Arrays.toString(act));
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void addTwo() {
        int[] array = new int[]{4343, -123};
        this.tree.init(array);
        int[] exp = {-123, 0, 4343};
        this.tree.add(0);
        int[] act = this.tree.toArray();
        this.tree.clear();
        System.out.println(Arrays.toString(act));
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void addOne() {
        int[] array = new int[]{232};
        this.tree.init(array);
        int[] exp = {-123, 232};
        this.tree.add(-123);
        int[] act = this.tree.toArray();
        this.tree.clear();
        System.out.println(Arrays.toString(act));
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void addEmpty() {
        int[] array = new int[]{};
        this.tree.init(array);
        int[] exp = {0};
        this.tree.add(0);
        int[] act = this.tree.toArray();
        this.tree.clear();
        System.out.println(Arrays.toString(act));
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void addTheSame() {
        int[] array = new int[]{232, 43432, 123, 543, 4343, 123, 5644, 34, 12, 44};
        this.tree.init(array);
        int[] exp = {1, 12, 34, 44, 123, 232, 543, 4343, 5644, 43432};
        this.tree.add(1);
        int[] act = this.tree.toArray();
        this.tree.clear();
        System.out.println(Arrays.toString(act));
        Assert.assertArrayEquals(exp, act);
    }

    //=================================================
    //=================== leaves ======================
    //=================================================

    @Test
    public void leavesMany() {
        int[] array = new int[]{1, 232, 43432, 123, 543, 4343, 125, 5644, 34, 12};
        this.tree.init(array);
        int exp = 3;
        int act = this.tree.leaves();
        this.tree.clear();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void leavesMany2() {
        int[] array = new int[]{1, 5, 0, -5, 4};
        this.tree.init(array);
        int exp = 2;
        int act = this.tree.leaves();
        this.tree.clear();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void leavesTwo() {
        int[] array = new int[]{1, 5};
        this.tree.init(array);
        int exp = 1;
        int act = this.tree.leaves();
        this.tree.clear();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void leavesOne() {
        int[] array = new int[]{1};
        this.tree.init(array);
        int exp = 1;
        int act = this.tree.leaves();
        this.tree.clear();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void leavesIsEmpty() {
        int[] array = new int[]{};
        this.tree.init(array);
        int exp = 0;
        int act = this.tree.leaves();
        this.tree.clear();
        Assert.assertEquals(exp, act);
    }


    //=================================================
    //=================== nodes ======================
    //=================================================

    @Test
    public void nodesMany() {
        int[] array = new int[]{1, 232, 43432, 123, 543, 4343, 125, 5644, 34, 12};
        this.tree.init(array);
        int exp = 10;
        int act = this.tree.nodes();
        this.tree.clear();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void nodesMany2() {
        int[] array = new int[]{1, 5, 0, -5, 4};
        this.tree.init(array);
        int exp = 5;
        int act = this.tree.nodes();
        this.tree.clear();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void nodesTwo() {
        int[] array = new int[]{1, 5};
        this.tree.init(array);
        int exp = 2;
        int act = this.tree.nodes();
        this.tree.clear();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void nodesOne() {
        int[] array = new int[]{1};
        this.tree.init(array);
        int exp = 1;
        int act = this.tree.nodes();
        this.tree.clear();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void nodesIsEmpty() {
        int[] array = new int[]{};
        this.tree.init(array);
        int exp = 0;
        int act = this.tree.nodes();
        this.tree.clear();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void nodesTheSame() {
        int[] array = new int[]{1, 5, 0, 5, 4, 4};
        this.tree.init(array);
        int exp = 4;
        int act = this.tree.nodes();
        this.tree.clear();
        Assert.assertEquals(exp, act);
    }

    //=================================================
    //=================== height ======================
    //=================================================

    @Test
    public void heightMany() {
        int[] array = new int[]{1, 232, 43432, 123, 543, 4343, 125, 5644, 34, 12};
        this.tree.init(array);
        int exp = 6;
        int act = this.tree.height();
        this.tree.clear();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void heightMany2() {
        int[] array = new int[]{1, 232, -6, 7, 0};
        this.tree.init(array);
        int exp = 3;
        int act = this.tree.height();
        this.tree.clear();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void heightTwo() {
        int[] array = new int[]{1, 232};
        this.tree.init(array);
        int exp = 2;
        int act = this.tree.height();
        this.tree.clear();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void heightOne() {
        int[] array = new int[]{1};
        this.tree.init(array);
        int exp = 1;
        int act = this.tree.height();
        this.tree.clear();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void heightIsEmpty() {
        int[] array = new int[]{};
        this.tree.init(array);
        int exp = 0;
        int act = this.tree.height();
        this.tree.clear();
        Assert.assertEquals(exp, act);
    }

    //=================================================
    //=================== width =======================
    //=================================================

    @Test
    public void widthMany() {
        int[] array = new int[]{1, 232, 43432, 123, -543, 4343, -125, 5644, 34, 12};
        this.tree.init(array);
        int exp = 3;
        int act = this.tree.width();
        this.tree.clear();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void widthMany2() {
        int[] array = new int[]{1, 0, 6, -5, 3};
        this.tree.init(array);
        int exp = 2;
        int act = this.tree.width();
        this.tree.clear();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void widthMany3() {
        int[] array = new int[]{0, 1, 2, 3, 4, 5, 6};
        this.tree.init(array);
        int exp = 1;
        int act = this.tree.width();
        this.tree.clear();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void widthTwo() {
        int[] array = new int[]{0, 1};
        this.tree.init(array);
        int exp = 1;
        int act = this.tree.width();
        this.tree.clear();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void widthOne() {
        int[] array = new int[]{0};
        this.tree.init(array);
        int exp = 1;
        int act = this.tree.width();
        this.tree.clear();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void widthIsEmpty() {
        int[] array = new int[]{};
        this.tree.init(array);
        int exp = 0;
        int act = this.tree.width();
        this.tree.clear();
        Assert.assertEquals(exp, act);
    }

    //=================================================
    //=================== reverse =====================
    //=================================================

    @Test
    public void reverseMany() {
        int[] array = new int[]{1, 2, 3, 4, 5};
        this.tree.init(array);
        int[] exp = {5, 4, 3, 2, 1};
        this.tree.reverse();
        int[] act = this.tree.toArray();
        this.tree.clear();
        System.out.println(Arrays.toString(act));
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void reverseMany2() {
        int[] array = new int[]{1, -3, 6, 77, 123, 0, -55};
        this.tree.init(array);
        int[] exp = {123, 77, 6, 1, 0, -3, -55};
        this.tree.reverse();
        int[] act = this.tree.toArray();
        this.tree.clear();
        System.out.println(Arrays.toString(act));
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void reverseTwo() {
        int[] array = new int[]{1, -3};
        this.tree.init(array);
        int[] exp = {1, -3};
        this.tree.reverse();
        int[] act = this.tree.toArray();
        this.tree.clear();
        System.out.println(Arrays.toString(act));
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void reverseTwo2() {
        int[] array = new int[]{-10, -3};
        this.tree.init(array);
        int[] exp = {-3, -10};
        this.tree.reverse();
        int[] act = this.tree.toArray();
        this.tree.clear();
        System.out.println(Arrays.toString(act));
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void reverseOne() {
        int[] array = new int[]{1};
        this.tree.init(array);
        int[] exp = {1};
        this.tree.reverse();
        int[] act = this.tree.toArray();
        this.tree.clear();
        System.out.println(Arrays.toString(act));
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void reverseIsEmpty() {
        int[] array = new int[]{};
        this.tree.init(array);
        int[] exp = {};
        this.tree.reverse();
        int[] act = this.tree.toArray();
        this.tree.clear();
        System.out.println(Arrays.toString(act));
        Assert.assertArrayEquals(exp, act);
    }

    //=================================================
    //=================== delete ======================
    //=================================================

    @Test
    public void deleteTwoLeaf() {
        int[] array = new int[]{232, 43432};
        this.tree.init(array);
        int[] exp = {232};
        this.tree.delete(43432);
        int[] act = this.tree.toArray();
        this.tree.clear();
        System.out.println(Arrays.toString(act));
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void deleteManyOneChild() {
        int[] array = new int[]{232, 43432, -123, 543, 4343, 123, 5644, 34, 12, 44};
        this.tree.init(array);
        int[] exp = {-123, 12, 34, 44, 123, 232, 543, 5644, 43432};
        this.tree.delete(4343);
        int[] act = this.tree.toArray();
        this.tree.clear();
        System.out.println(Arrays.toString(act));
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void deleteManyTwoChild() {
        int[] array = new int[]{232, 43432, -123, 543, 4343, 123, 5644, 34, 12, 44};
        this.tree.init(array);
        int[] exp = {-123, 12, 44, 123, 232, 543, 4343, 5644, 43432};
        this.tree.delete(34);
        int[] act = this.tree.toArray();
        this.tree.clear();
        System.out.println(Arrays.toString(act));
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void deleteManyRoot() {
        int[] array = new int[]{232, 43432, -123, 543, 4343, 123, 5644, 34, 12, 44};
        this.tree.init(array);
        int[] exp = {-123, 12, 34, 44, 123, 543, 4343, 5644, 43432};
        this.tree.delete(232);
        int[] act = this.tree.toArray();
        this.tree.clear();
        System.out.println(Arrays.toString(act));
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void deleteManyLeaf() {
        int[] array = new int[]{232, 43432, -123, 543, 4343, 123, 5644, 34, 12, 44};
        this.tree.init(array);
        int[] exp = {-123, 12, 34, 123, 232, 543, 4343, 5644, 43432};
        this.tree.delete(44);
        int[] act = this.tree.toArray();
        this.tree.clear();
        System.out.println(Arrays.toString(act));
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void deleteThreeOneChild() {
        int[] array = new int[]{14, 33, 66};
        this.tree.init(array);
        int[] exp = {14,66};
        this.tree.delete(33);
        int[] act = this.tree.toArray();
        this.tree.clear();
        System.out.println(Arrays.toString(act));
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void deleteThreeRoot() {
        int[] array = new int[]{6, 99, 4};
        this.tree.init(array);
        int[] exp = {4, 99};
        this.tree.delete(6);
        int[] act = this.tree.toArray();
        this.tree.clear();
        System.out.println(Arrays.toString(act));
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void deleteTreeTwoChild() {
        int[] array = new int[]{0, 7, 6, 9};
        this.tree.init(array);
        int[] exp = {0, 6, 9};
        this.tree.delete(7);
        int[] act = this.tree.toArray();
        this.tree.clear();
        System.out.println(Arrays.toString(act));
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void deleteRoot() {
        int[] array = new int[]{232};
        this.tree.init(array);
        int[] exp = {};
        this.tree.delete(232);
        int[] act = this.tree.toArray();
        this.tree.clear();
        System.out.println(Arrays.toString(act));
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void deleteOneLeaf(){
        int[] array = new int[]{232, 100};
        this.tree.init(array);
        int[] exp = {232};
        this.tree.delete(100);
        int[] act = this.tree.toArray();
        this.tree.clear();
        System.out.println(Arrays.toString(act));
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void deleteOne(){
        int[] array = new int[]{232, 100};
        this.tree.init(array);
        int[] exp = {100};
        this.tree.delete(232);
        int[] act = this.tree.toArray();
        this.tree.clear();
        System.out.println(Arrays.toString(act));
        Assert.assertArrayEquals(exp, act);
    }

    //=================================================
    //=================== Add =========================
    //=================================================

    @Test
    public void printMany() {
        int[] array = new int[]{232, 43432, -123, 543, 4343, 123, 5644, 34, 12, 44};
        this.tree.init(array);
        int[] exp = {-123, 12, 34, 44, 123, 232, 543, 4343, 5644, 43432};
        this.tree.print();
        int[] act = this.tree.toArray();
        this.tree.clear();
        System.out.println(Arrays.toString(act));
        Assert.assertArrayEquals(exp, act);
    }
}