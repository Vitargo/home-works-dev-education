package org.bitbucket.custom.server.nio;

import org.junit.Test;

import java.io.IOException;
import java.util.Map;

public class SimpleHttpRequestTest {

    @Test
    public void getRequest() throws IOException {
        String request = "POST / HTTP/1.1\n" +
                "Content-Type: text/plain\n" +
                "User-Agent: PostmanRuntime/7.26.10\n" +
                "Accept: */*\n" +
                "Postman-Token: 46258635-bfdf-4fd1-8103-1d97d3f42cea\n" +
                "Host: localhost:8083\n" +
                "Accept-Encoding: gzip, deflate, br\n" +
                "Connection: keep-alive\n" +
                "Content-Length: 4\n" +
                "\n" +
                "\n" +
                "Vita";
        Map<String, String> exp = Map.of("Accept", "*/*",
                "TypeRequest:", "POST",
                "User-Agent", "PostmanRuntime/7.26.10",
                "Connection", "keep-alive",
                "Postman-Token", "46258635-bfdf-4fd1-8103-1d97d3f42cea",
                "Host", "localhost:8083",
                "Accept-Encoding", "gzip deflate, br",
                "Content-Length", "4",
                "Body:", "Vita",
                "Content-Type", "text/plain");
        Map<String, String> act = SimpleHttpRequest.parseRequest(request);
        System.out.println(exp);
        System.out.println(act);
    }

}