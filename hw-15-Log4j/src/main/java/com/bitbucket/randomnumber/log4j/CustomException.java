package com.bitbucket.randomnumber.log4j;

public class CustomException extends RuntimeException{

    public CustomException(String message) {
        super(message);
    }
}
