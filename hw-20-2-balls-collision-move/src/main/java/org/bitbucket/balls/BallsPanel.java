package org.bitbucket.balls;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Arrays;

public class BallsPanel extends JPanel {

    private static ArrayList<Ball> balls = new ArrayList<>();

    private static ArrayList<Ball[]> checkCollision = new ArrayList<>();

    private static int counterBalls = 0;

    public static int getCounterBalls() {
        return counterBalls;
    }

    public BallsPanel() {
        setLayout(null);
        addMouseListener(new MouseListener(){

            @Override
            public void mouseClicked(MouseEvent e) {

            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {
                Point point = e.getPoint();
                Ball ball = new Ball(point, BallsPanel.this);
                checkTheBall(ball);
                add(ball);
                counterBalls++;
                //System.out.println("Name of new thread: " + counterBalls);
                Thread t = new Thread(ball, "ball-" + counterBalls);
                t.start();
            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });
        setVisible(Boolean.TRUE);
    }

    private void checkBounds(Ball ball) {
        if(ball.getX() + ball.getRadius() >= getWidth()){
            ball.setX(ball.getX() - ball.getRadius());
        }
        if(ball.getY() + ball.getRadius() >= getHeight()){
            ball.setY(ball.getY() - ball.getRadius());
        }
        if(ball.getX() < 10){
            ball.setX(ball.getX() + 10);
        }
        if(ball.getY() < 10){
            ball.setY(ball.getY() + 10);
        }
    }

    private void checkTheBall(Ball ball) {
        checkBounds(ball);
        for(Ball anotherBall:balls){
            int nPoints = 4;
            int[] xPoints = {anotherBall.getX(), anotherBall.getX()+anotherBall.getRadius(), anotherBall.getX()+anotherBall.getRadius(), anotherBall.getX()};
            int[] yPoints = {anotherBall.getY(), anotherBall.getY(), anotherBall.getY()+anotherBall.getRadius(), anotherBall.getY()+anotherBall.getRadius()};
            Polygon polygon = new Polygon(xPoints, yPoints, nPoints);
            boolean fact = polygon.contains(ball.getX(), ball.getY());
            if(fact){
                System.out.println("++++++++++++++++++++" + fact);
                ball.setX(ball.getX() - ball.getRadius());
                ball.setY(ball.getY() + ball.getRadius());
            }
        }
    }

    private void checkTwoBalls(Ball ball1, Ball ball2) {
            Polygon polygon = createPolygon(ball2);
            boolean fact = checkPolygon(ball1, polygon);
            while(fact){
                ball1.setX(ball1.getX() + 5);
                ball1.setY(ball1.getY() + 5);
                checkBounds(ball1);
                System.out.println(ball1.getX() + " " + ball1.getY());
                fact = checkPolygon(ball1, polygon);
            }
    }

    private Polygon createPolygon(Ball ball2) {
        int[] xPoints = {ball2.getX(), ball2.getX()+ball2.getRadius(), ball2.getX()+ball2.getRadius(), ball2.getX()};
        int[] yPoints = {ball2.getY(), ball2.getY(), ball2.getY()+ball2.getRadius(), ball2.getY()+ball2.getRadius()};
        return new Polygon(xPoints, yPoints, 4);
    }

    private boolean checkPolygon(Ball ball1, Polygon polygon){
        int x = ball1.getX();
        int y = ball1.getY();
        System.out.println( x + " " + y);
        return polygon.contains(x, y);
    }

    public synchronized void addPointToList(Ball ball, String threadName){
        balls.add(ball);
        //System.out.println("ball added from thread: " + threadName);
        while (balls.size() < BallsPanel.getCounterBalls()){

            try {
                //System.out.println(threadName + " waiting! Balls in list = " + balls.size() + ", size of list = " + BallsPanel.getCounterBalls());
                wait();
                //System.out.println(threadName + " get up!!");
                //System.out.println(threadName );
                return;
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        //System.out.println("------" + Arrays.toString(balls.toArray()));
        //System.out.println(threadName + " notifyAll! Balls in list = " + balls.size() + ", size of list = " + BallsPanel.getCounterBalls());
        checkCollision = checkCollision();
        //System.out.println(Arrays.toString(checkCollision.toArray()));
        resolveCollision(checkCollision);
        //System.out.println(Arrays.toString(closeThread.toArray()));
        notifyAll();
        balls.clear();
    }

    public ArrayList<Ball[]>  checkCollision() {
        int size = balls.size();
        ArrayList<Ball[]> collisionBalls = new ArrayList<>();
        for (int i = 0; i < size; i++){
            for(int j = i+1; j < size; j++){
                double ri = balls.get(i).getRadius()/2;
                double rj = balls.get(j).getRadius()/2;
                double ax = (balls.get(i).getX()+ri) - (balls.get(j).getX()+rj);
                double ay = (balls.get(i).getY()+ri) - (balls.get(j).getY()+rj);
                double a = Math.pow(ax, 2);
                double b = Math.pow(ay, 2);
                double c = a + b;
                double d = Math.pow((ri + rj), 2);
                if (c < d) {
                    checkTwoBalls(balls.get(i), balls.get(j));
                    System.out.println("col ball1");
                    checkTwoBalls(balls.get(j), balls.get(i));
                    System.out.println("col ball2");
                    checkBounds(balls.get(i));
                    checkBounds(balls.get(j));
                    collisionBalls.add(new Ball[]{balls.get(i), balls.get(j)});
                    //System.out.println(a + " " + b + " " + c + " " +d);
                    //System.out.println(balls.get(i).toString() + balls.get(j).toString());
                }
            }
        }
        //System.out.println(Arrays.toString(collisionBalls.toArray()));
        return collisionBalls;
    }

    private void resolveCollision (ArrayList < Ball[]> checkCollision){
        for (Ball[] balls : checkCollision) {
            int dx0 = balls[0].getDx();
            int dy0 = balls[0].getDy();
            int dx1 = balls[1].getDx();
            int dy1 = balls[1].getDy();

            int x0 = balls[0].getX();
            int y0 = balls[0].getY();
            int x1 = balls[1].getX();
            int y1 = balls[1].getY();
            if (x0 < x1 && y0 < y1) {
                balls[0].setDx(-dy0);
                balls[0].setDy(dx0);
                balls[1].setDx(-dy1);
                balls[1].setDy(dx1);
            } else if (x0 < x1 && y0 > y1) {
                balls[0].setDx(-dy0);
                balls[0].setDy(dx0);
                balls[1].setDx(-dy1);
                balls[1].setDy(dx1);
            } else if (x0 == x1){
                balls[0].setDy(-dy0);
                balls[1].setDy(-dy1);
            } else if (y0 == y1) {
                balls[0].setDx(-dx0);
                balls[1].setDx(-dx1);
            }
        }
    }
}
