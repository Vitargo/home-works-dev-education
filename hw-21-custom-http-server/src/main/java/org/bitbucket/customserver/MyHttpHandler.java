package org.bitbucket.customserver;

import java.io.*;
import java.net.Socket;

public class MyHttpHandler {

    private SimpleHttpRequest req;

    private SimpleHttpResponse resp;

    private Socket socket;

    public MyHttpHandler(Socket socket) {
        this.socket = socket;
        this.req = new SimpleHttpRequest(socket);
        this.resp = new SimpleHttpResponse();
    }

    public void formatResponse() throws IOException {
        String typeRequest = this.req.getRequestType();
        if(typeRequest.equals("GET")){
            doGet(this.req, this.resp);
        } else if (typeRequest.equals("PUT")){
            doPut(this.req, this.resp);
        } else if (typeRequest.equals("POST")){
            doPost(this.req, this.resp);
        } else if (typeRequest.equals("DELETE")) {
            doDelete(this.req, this.resp);
        } else {
            doOther(this.req, this.resp);
        }
    }

    public void doGet(SimpleHttpRequest req, SimpleHttpResponse resp) throws IOException {
        OutputStream out = this.socket.getOutputStream();
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(out));
        String response = SimpleHttpResponse.responseOnGet();
        bw.write(response);
        bw.flush();
    }

    public void doPost(SimpleHttpRequest req, SimpleHttpResponse resp) throws IOException {
        OutputStream out = this.socket.getOutputStream();
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(out));
        String body = req.getBody();
        String name = getName(body);
        String greeting = getGreetings(body);
        String response = SimpleHttpResponse.responseOnPost(name, greeting);
        bw.write(response);
        bw.flush();
    }

    public void doPut(SimpleHttpRequest req, SimpleHttpResponse resp) throws IOException {
        OutputStream out = this.socket.getOutputStream();
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(out));
        String response = SimpleHttpResponse.responseOnPut();
        bw.write(response);
        bw.flush();
    }

    public void doDelete(SimpleHttpRequest req, SimpleHttpResponse resp) throws IOException {
        OutputStream out = this.socket.getOutputStream();
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(out));
        String response = SimpleHttpResponse.responseOnDelete();
        bw.write(response);
        bw.flush();
    }

    public void doOther(SimpleHttpRequest req, SimpleHttpResponse resp) throws IOException {
        OutputStream out = this.socket.getOutputStream();
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(out));
        String response = SimpleHttpResponse.responseOnOther();
        bw.write(response);
        bw.flush();
    }

    public static String getName(String body) {
        int str = body.indexOf("&to=");
        String name = body.substring(str+4);
        return name;
    }

    public static String getGreetings(String body) {
        int f = body.indexOf("&to=");
        int s = body.indexOf("say=");
        String name = body.substring(s+4, f);
        return name;
    }
}